<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {


	public function index()
	{
		$this->load->view('head');
		$category = $this->bml_read_json->readRentMenu();
		$data['category'] = $category;
		$faq = $this->bml_read_json->readFaqs();
		$data['faq'] = $faq;


		$this->load->view('header',$data);
		$this->load->view('faq');
		$this->load->view('footer_analytics');
	}
	public function app()
	{
		$faq = $this->bml_read_json->readFaqs();
		$data['faq'] = $faq;
		$this->load->view('head');
		$this->load->view('faq',$data);
	}
	
}
