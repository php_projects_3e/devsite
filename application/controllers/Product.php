<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Product extends CI_Controller {



  public function view($productName)

  {

    $productName = bml_urldecode($productName);



    $this->load->view('head');

    $data['category'] = $this->bml_read_json->readRentMenu();



    //get product details

    $this->load->model('productModel');

    $productDetailsResult = $this->productModel->getProductDetails($productName);

    $productDetails = (array_key_exists(0, $productDetailsResult)) ? $productDetailsResult[0]: array();//get first resultset

    $productDetails = (array_key_exists(0, $productDetails)) ? $productDetails[0]: array();//get first row

    if(!empty($productDetails))

    {

        $resultSet = $this->productModel->getPriceByProductIDAndSeasonID($productDetails['itemId'], $productDetails['seasonID']);

        $productDetails['priceList'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();

        $data['productDetails'] = $productDetails;



        //breadcrumb

        $breadcrumb = [];

        $breadcrumb[] = array('link' => site_url(), 'name'=>'Home');

        $breadcrumb[] = array('link' => site_url('productlist/category/'.urlencode($productDetails['category_Name'])), 'name'=>$productDetails['category_Name']);

        $breadcrumb[] = array('link' => site_url('productlist/brand/'.urlencode($productDetails['category_Name']).'/'.urlencode($productDetails['itemBrandName'])), 'name'=>$productDetails['itemBrandName']);

        $breadcrumb[] = array('link' => '', 'name'=>$productDetails['itemName']);



    }

    else {

        //breadcrumb

        $breadcrumb = [];

        $breadcrumb[] = array('link' => site_url(), 'name'=>'Home');

        $breadcrumb[] = array('link' => '', 'name'=>$productName);

    }

    $data['breadcrumb'] = $breadcrumb;



    $this->load->view('header',$data);

    $this->load->view('productview');

    $this->load->view('footer');

  }



  public function search()

  {

    if (isset($_GET['term'])){

      $q = strtolower($_GET['term']);

      $q = str_replace(' ', '%',$q);

      $sql = "SELECT b.itemBrandName, c.category_Name as category_Name, a.`itemName`, d.price

            FROM `itemmaster` a

            JOIN tbl_item_brand b

            on a.`itemBrand` = b.itemBrandID

            JOIN category_tb c

            ON a.`itemCategory` = c.categoryID

            JOIN pricemaster d

            ON a.itemId = d.itemId

            JOIN (select itemId, min(days) as days from pricemaster group by itemId) t1

            ON a.itemId = t1.itemId

            WHERE a.`itemName` LIKE '%$q%'

            and d.days = t1.days

            and a.seasonID = d.seasonId

            and a.`itemStatus` = 1";

      $result = $this->bml_database->getResults($sql);

      $result = (array_key_exists(0, $result) ? $result[0]: array());



      $searchResTmp = [];

      foreach ($result as $resultRow) {

        $searchResTmp[$resultRow['category_Name']][] = array('link' => site_url('product/view/'.bml_urlencode($resultRow['itemName'])), 'label2' => "<span class='fa fa-inr'>".$resultRow['price']."</span>", 'label1' => $resultRow['itemName'], 'spanStyle' => "style = ' font-size:14px:font-weight: bold;padding-left: 30px;'");

      }

      //$n=3;

      $searchRes = [];

      foreach ($searchResTmp as $categoryName => $row) {

        $searchRes[] = array('link' => site_url('productlist/category/'.$categoryName), 'label2' => 'view all', 'label1' => "In ".$categoryName, 'spanStyle' => "style = font-size:18px;color:#C1272D;font-weight: bold;margin-top: 50px;");

        //$i = 1;

        foreach ($row as $row1) {

          $searchRes[] = $row1;

          //if($i == $n) break;

          //$i++;

        }

      }

      echo json_encode($searchRes);

    }

  }



  public function rent($productName, $productRowID = '')

  {

    $productName = bml_urldecode($productName);

    $productInCart = array();

    if($productRowID != '')

    {

        $productInCart = $this->cart->get_item($productRowID);

    }

    

    $data['productInCart'] = $productInCart;



    $this->load->view('head');

    $data['category'] = $this->bml_read_json->readRentMenu();



    //get login status

    $data['isLoggedIn'] = $this->user_session->isLoggedIn();

    $data['isApproved'] = $this->user_session->getSessionVar('is_approved');



    //get product details

    $this->load->model('productModel');

    $productDetailsResult = $this->productModel->getProductDetails($productName);

  

    $productDetails = (array_key_exists(0, $productDetailsResult)) ? $productDetailsResult[0]: array();//get first resultset

    $productDetails = (array_key_exists(0, $productDetails)) ? $productDetails[0]: array();//get first row

    

    $resultSet = $this->productModel->getPriceByProductIDAndSeasonID($productDetails['itemId'], $productDetails['seasonID']);

    $productDetails['priceList'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();

    $data['productDetails'] = $productDetails;

   

    $this->load->model('orderModel');

    $availableCountRes = $this->orderModel->getAvailableQtyByItemId($productDetails['itemId']);

    

    $availableQty = (array_key_exists(0, $availableCountRes)) ? $availableCountRes[0]: array();





    $totalQty = (array_key_exists(1, $availableCountRes)) ? $availableCountRes[1][0]['totalQty']: array();

  

    $this->load->library('cart');

    $cartDetails = $this->cart->get_item_by_id($productDetails['itemId']);

    $unAvailableQtyArr = array();



    foreach ($availableQty as $availableQtyRow) {

        $availableQtyRow['dates'];

       

        if($availableQtyRow['availableQty'] <= 0){

            $unAvailableQtyArr[] = date_format(date_create($availableQtyRow['dates']), "Y-m-d");

        }

        else{

            $date = date_create_from_format("Y-m-d", $availableQtyRow['dates']);

            

            $qty = $availableQtyRow['availableQty'];

            foreach ($cartDetails as $cartRow) {

              if($productRowID == $cartRow['rowid'])

              {

                continue;

              }

              $itemOptions = $cartRow['options'];

              if($itemOptions['waitingList']) continue;

              if(date_create_from_format('d/M/Y', $itemOptions['startDate']) <= $date && date_create_from_format('d/M/Y', $itemOptions['endDate']) >= $date)

              {

                $qty -= $cartRow['qty'];

                if($qty <= 0)

                {

                    $unAvailableQtyArr[] = date_format($date, "Y-m-d");

                    break;

                }

              }

            }

        }

    }

    $data['unAvailableDates'] = $unAvailableQtyArr;



    $holidayListRes = $this->orderModel->getHolidayList();

    $holidayList = (array_key_exists(0, $holidayListRes)) ? $holidayListRes[0]: array();



    $holidayListArr = array();

    foreach ($holidayList as $holidayListRow) {

        $holidayListArr[] = $holidayListRow['holidayDate'];

    }

    $data['holidayList'] = $holidayListArr;





    //breadcrumb

    $breadcrumb = [];

    $breadcrumb[] = array('link' => site_url(), 'name'=>'Home');

    $breadcrumb[] = array('link' => site_url('productlist/category/'.urlencode($productDetails['category_Name'])), 'name'=>$productDetails['category_Name']);

    $breadcrumb[] = array('link' => site_url('productlist/brand/'.urlencode($productDetails['category_Name']).'/'.urlencode($productDetails['itemBrandName'])), 'name'=>$productDetails['itemBrandName']);

    $breadcrumb[] = array('link' => site_url('product/view/'.$productDetails['itemName']), 'name'=>$productDetails['itemName']);

    $breadcrumb[] = array('link' => '', 'name'=>'Rent Now');

    $data['breadcrumb'] = $breadcrumb;



    $this->load->view('header',$data);

    $this->load->view('productrent');

    $this->load->view('footer_analytics');

  }



}

