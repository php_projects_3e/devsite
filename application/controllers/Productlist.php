<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productlist extends CI_Controller {
	public function index(){
		echo "test file";
	}

  public function category($categoryNames)
  {
  	$this->load->library('pagination');
		$this->load->view('head');
		$data['category'] = $this->bml_read_json->readRentMenu();
		$categoryNames = str_replace(';',',',bml_urldecode($categoryNames));

		//get rent menu from json
		$this->load->model('commonModel');
		$resultSet = $this->commonModel->getMainPageCategories();
		$data['mainPageCategory'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();

		$this->load->model('productModel');

		//pagination
/*		$config['total_rows'] = $this->productModel->getProductCountByCategoryName($categoryNames);
		$params['order_by'] = $this->input->get('order_by');
		if($params['order_by']){
			list($order, $orderType) = explode(" ",$params['order_by']);
		}
		else {
			list($order, $orderType) = array('itemName','asc');
		}
		$params['status'] = $this->input->get('status');
		$perPage = 9;
		$config['url_format'] = site_url("productlist/category/$categoryNames/{offset}?".http_build_query($params));
		$config['per_page'] = 5;
		$this->pagination->initialize($config);
		$offset = $this->pagination->getOffset();

		$data['paginationButtons'] = $this->pagination->create_links();
*/
		//get product list
		$productListResult = $this->productModel->getProductListByCategoryName($categoryNames);
		$productlistArr = (array_key_exists(0, $productListResult)) ? $productListResult[0]: array();

		//get price for each product.
		foreach ($productlistArr as $key => $productlistRow) {
		  $resultSet = $this->productModel->getPriceByProductIDAndSeasonID($productlistRow['itemId'], $productlistRow['seasonID']);
		  $pricelistArr = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
		  $productlistArr[$key]['priceList'] = $pricelistArr;
		}

		//breadcrumb
		$breadcrumb = [];
		$breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
		$breadcrumb[] = array('link' => '', 'name'=>$categoryNames);

		$data['breadcrumb'] = $breadcrumb;
		$data['productlistArr'] = $productlistArr;
		//print_r($productlistArr);
		$this->load->view('header',$data);
		$this->load->view('productlist');
		$this->load->view('footer');
 	}

	public function featured()
  {
		$this->load->view('head');
		
		$data['category'] = $this->bml_read_json->readRentMenu();

		//get rent menu from json
		$this->load->model('commonModel');
		$resultSet = $this->commonModel->getMainPageCategories();
		$data['mainPageCategory'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();

		//get product list
		$this->load->model('productModel');
		$productListResult = $this->productModel->getFeaturedProductList();
		$productlistArr = (array_key_exists(0, $productListResult)) ? $productListResult[0]: array();

		//get price for each product.
		foreach ($productlistArr as $key => $productlistRow) {
		  $resultSet = $this->productModel->getPriceByProductIDAndSeasonID($productlistRow['itemId'], $productlistRow['seasonID']);
		  $pricelistArr = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
		  $productlistArr[$key]['priceList'] = $pricelistArr;
		}

		//breadcrumb
		$breadcrumb = [];
		$breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
		$breadcrumb[] = array('link' => '', 'name'=>"Featured Equipment");

		$data['breadcrumb'] = $breadcrumb;
		$data['productlistArr'] = $productlistArr;
		//print_r($productlistArr);
		$this->load->view('header',$data);
		$this->load->view('productlist');
		$this->load->view('footer');
	 }

  public function brand($categoryName,$brands)
  {
		$categoryName = bml_urldecode($categoryName);
		$brands = bml_urldecode($brands);

		$this->load->view('head');
		$data['category'] = $this->bml_read_json->readRentMenu();

		//get rent menu from json
		$this->load->model('commonModel');
		$resultSet = $this->commonModel->getMainPageCategories();
		$data['mainPageCategory'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();

		//get product list
		$this->load->model('productModel');
		$productListResult = $this->productModel->getProductListByCategoryAndBrand($categoryName, $brands);
		$productlistArr = (array_key_exists(0, $productListResult)) ? $productListResult[0]: array();

		//get price for each product.
		foreach ($productlistArr as $key => $productlistRow) {
		  $resultSet = $this->productModel->getPriceByProductIDAndSeasonID($productlistRow['itemId'], $productlistRow['seasonID']);
		  $pricelistArr = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
		  $productlistArr[$key]['priceList'] = $pricelistArr;
		}

		//breadcrumb
		$breadcrumb = [];
		$breadcrumb[] = array('link' => site_url(), 'name'=>'Home');
		$breadcrumb[] = array('link' => site_url('productlist/category/'.$categoryName), 'name'=>bml_urldecode($categoryName));
		$breadcrumb[] = array('link' => '', 'name'=>bml_urldecode($brands));

		$data['breadcrumb'] = $breadcrumb;
		$data['productlistArr'] = $productlistArr;
		//print_r($productlistArr);
		$category = $this->bml_read_json->readRentMenu();
		$data['category'] = $category;
		$this->load->view('header',$data);
		$this->load->view('productlist');
		$this->load->view('footer');
  }
}
