<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {


	public function index()
	{
		$this->load->view('head');
		$category = $this->bml_read_json->readRentMenu();
		$data['category'] = $category;
		$faq = $this->bml_read_json->readFaqs();
		$data['faq'] = $faq;


		$this->load->view('header',$data);

		$this->load->view('gallery');
		$this->load->view('footer');
	}
}
