<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function index()
  {
    if($this->session->userdata('is_admin_login'))
    {
      redirect(admin_url("orders/booked"));
    }
    else
    {
      redirect(site_url());
    }
  }
  public function deleteMeOrders()
  {
	$customerId = 5466;
	$sql = "SELECT `subOrderID`,`orderID` FROM `tbl_suborder` WHERE `orderID` in (SELECT `orderId` FROM `ordertb` WHERE `customerId` = '$customerId')";
	$res = $this->bml_database->getResults($sql);
	$res = $res[0];
	print_r(json_encode($res));
	
	$orders = array();
	foreach($res as $row)
	{
		$orders[$row['orderID']][] = $row;
	}
	
	foreach($orders as $orderId => $row1)
	{
		foreach($row1 as $row)
		{
			$sql1= "DELETE FROM `orderproduct` WHERE `subOrderID`='".$row['subOrderID']."'";
			$sql2= "DELETE FROM `tbl_suborder` WHERE `subOrderID`='".$row['subOrderID']."'";
			$this->bml_database->getResults($sql1);
			$this->bml_database->getResults($sql2);
		}
		$sql3= "DELETE FROM `ordertb` WHERE `orderId`='".$orderId."'";
		$this->bml_database->getResults($sql3);
			
	}
  }
}
