<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->helper('bml_util');
  }

  public function approved()
  {
    $data = array("title" => "Clients List", "subTitle" => "Approved","sidebarCollapse" => true);
    $data['tableHeader'] = array('', 'Client Number','Name','Email ID', 'Mobile Number', 'Modified Date', 'Registered Date', 'Status','');

    $result = $this->adminModel->getClientsByStatus("1", "`customerImagePath`, `customerNumber`, concat(`firstName`,' ', `lastName`), `emailId`, `mobileNumber`, `lastUpdateDate`, `registeredDate`, `status`,`customerImagePath`");

    $result = (array_key_exists(0, $result))? $result[0]: array();
    $count = count($result);

    for($i=0;$i < $count; $i++)
    {
      $result[$i]['status'] = "Approved";
      $result[$i][] = "<a href='".admin_url('clients/view/'.$result[$i]['customerNumber'])."'><button>View</button></a>";
      $result[$i]['customerImagePath'] = generateAdminClientURL($result[$i]['customerImagePath']);
    }

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/clientList');
    $this->load->view('admin/footer');
  }

  public function unapproved()
  {
    $data = array("title" => "Clients List", "subTitle" => "UnApproved","sidebarCollapse" => true);

    $data['tableHeader'] = array('', 'Client #', 'Name', 'Email ID', 'Mobile Number', 'PAN', 'Addr Proof', 'ID Proof', 'Modified Date', 'Registered Date', 'Status','');

    $result = $this->adminModel->getClientsByStatus("0", "`customerImagePath`, `customerNumber`, CONCAT(`firstName`,' ', `lastName`), `emailId`, `mobileNumber`, `pancardNumber`, `addressProof`, `idProof`, `lastUpdateDate`, `registeredDate`, `status`", true);

    $result = (array_key_exists(0, $result))? $result[0]: array();
    $count = count($result);

    for($i=0;$i < $count; $i++)
    {
      $result[$i]['status'] = "unapproved";

      $result[$i][] = "<a href='".admin_url('clients/view/'.$result[$i]['customerNumber'])."'><button>View</button></a>";

      $result[$i]['customerImagePath'] = generateAdminClientURL($result[$i]['customerImagePath']);

      $result[$i]['addressProof'] = generateAdminFileDownload($result[$i]['addressProof']);
      $result[$i]['idProof'] = generateAdminFileDownload($result[$i]['idProof']);
    }
    $data['tableRows'] = $result;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/clientList');
    $this->load->view('admin/footer');
  }

  public function unapprovedall()
  {
    $data = array("title" => "Clients List", "subTitle" => "UnApproved","sidebarCollapse" => true);

    $data['tableHeader'] = array('', 'Client #', 'Name', 'Email ID', 'Mobile Number', 'PAN', 'Addr Proof', 'ID Proof', 'Modified Date', 'Registered Date', 'Status','');

    $result = $this->adminModel->getClientsByStatus("0", "`customerImagePath`, `customerNumber`, CONCAT(`firstName`,' ', `lastName`), `emailId`, `mobileNumber`, `pancardNumber`, `addressProof`, `idProof`, `lastUpdateDate`, `registeredDate`, `status`", false);

    $result = (array_key_exists(0, $result))? $result[0]: array();
    $count = count($result);

    for($i=0;$i < $count; $i++)
    {
      $result[$i]['status'] = "unapproved";

      $result[$i][] = "<a href='".admin_url('clients/view/'.$result[$i]['customerNumber'])."'><button>View</button></a>";

      $result[$i]['customerImagePath'] = generateAdminClientURL($result[$i]['customerImagePath']);

      $result[$i]['addressProof'] = generateAdminFileDownload($result[$i]['addressProof']);
      $result[$i]['idProof'] = generateAdminFileDownload($result[$i]['idProof']);
    }
    $data['tableRows'] = $result;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/clientList');
    $this->load->view('admin/footer');
  }
  
  public function changeapprove()
  {
    $data = array("title" => "Clients List", "subTitle" => "Approve Changes","sidebarCollapse" => true);
    $data['tableHeader'] = array('', 'Client Number','Name','Email ID', 'Mobile Number', 'Modified Date', 'Registered Date', 'Status','');

    $result = $this->adminModel->getClientsByStatus("3", "`customerImagePath`, `customerNumber`, concat(`firstName`,' ', `lastName`), `emailId`, `mobileNumber`, `lastUpdateDate`, `registeredDate`, `status`,`customerImagePath`");

    $result = (array_key_exists(0, $result))? $result[0]: array();
    $count = count($result);

    for($i=0;$i < $count; $i++)
    {
      $result[$i]['status'] = "Change Request";
      $result[$i][] = "<a href='".admin_url('clients/view/'.$result[$i]['customerNumber'])."'><button>View</button></a>";
      $result[$i]['customerImagePath'] = generateAdminClientURL($result[$i]['customerImagePath']);
    }

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/clientList');
    $this->load->view('admin/footer');
  }

  public function blocked()
  {
    $data = array("title" => "Clients List", "subTitle" => "Blocked","sidebarCollapse" => true);
    $data['tableHeader'] = array('', 'Client Number','Name','Email ID', 'Mobile Number', 'Modified Date', 'Registered Date', 'Status','');

    $result = $this->adminModel->getClientsByStatus("2", "`customerImagePath`, `customerNumber`, concat(`firstName`,' ', `lastName`), `emailId`, `mobileNumber`, `lastUpdateDate`, `registeredDate`, `status`,`customerImagePath`");

    $result = (array_key_exists(0, $result))? $result[0]: array();
    $count = count($result);

    for($i=0;$i < $count; $i++)
    {
      $result[$i]['status'] = "Blocked";
      $result[$i][] = "<a href='".admin_url('clients/view/'.$result[$i]['customerNumber'])."'><button>View</button></a>";
      $result[$i]['customerImagePath'] = generateAdminClientURL($result[$i]['customerImagePath']);
    }

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/clientList');
    $this->load->view('admin/footer');
  }

  public function view($clientNumber)
  {
    $data = array("title" => "Clients View", "subTitle" => " ","sidebarCollapse" => true);

    $result = $this->adminModel->getClientDetailsByClientNumber($clientNumber);
    
    $data['customerDetails']= (array_key_exists(1, $result))? $result[1][0]: array();

    $resultSet1=(array_key_exists(1, $result))? $result[1][0]: array();
    if($resultSet1['companyPerson']=='C')
    {
      $sql="SELECT * FROM `company_details` WHERE `companyID`='".$resultSet1['companyID']."'";
      $copanyDetailsResult=$this->bml_database->getResults($sql);
      $data['companyAddressDetails'] = (array_key_exists(0, $copanyDetailsResult))? $copanyDetailsResult[0][0]: array();

    }


    $addreessResult = (array_key_exists(2, $result))? $result[2]: array();

    $address = [];
    foreach ($addreessResult as $addreessResultRow) {
      $address[$addreessResultRow['addressType']] = $addreessResultRow;
    }
    $data['address'] = $address;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/clientView');
    $this->load->view('admin/footer');
  }

  public function addressAction()
  {
    $addressID = $this->input->post('addressID');
	$type = $this->input->post('type');
	$shippingcost = $this->input->post('shippingcost');
	$addressLandmark = $this->input->post('addressLandmark');
	$addressPin = $this->input->post('addressPin');
	$addressState = $this->input->post('addressState');
	$addressCity = $this->input->post('addressCity');
	$addressLine2 = $this->input->post('addressLine2');
	$addressLine1 = $this->input->post('addressLine1');
	$customerNumber = $this->input->post('customerNumber');
	
	if($addressID == '')
	{
		$sql = "SELECT `customerId` INTO @custID FROM `customer` WHERE `customerNumber` = '$customerNumber';";
		$sql .= "INSERT INTO `customer_address`(`customerId`, `addressLine1`, `addressLine2`, `addressCity`, `addressState`, `addressPin`, `addressLandmark`, `addressType`, `shippingCost`, `status`)
				values (@custID, '$addressLine1', '$addressLine2', '$addressCity', '$addressState', '$addressPin', '$addressLandmark', '$type', '$shippingcost', 0)";
		$this->bml_database->getResults($sql);
		header('Location: '.$this->agent->referrer());
	}
    $shippingcost = $this->input->post('shippingcost');
    $addrButton = $this->input->post('addrButton');

    $status = '`status`';
    switch ($addrButton) {
      case 'approve':
        $status = '1';
        break;
      case 'unapprove':
        $status = '0';
        break;
      default:
        $status = '`status`';
        break;
    }
	$sql = "UPDATE `customer_address` 
			set `addressLine1` = ".$this->db->escape($addressLine1).", 
				`addressLine2` = ".$this->db->escape($addressLine2).", 
				`addressCity`=".$this->db->escape($addressCity).", 
				`addressState`=".$this->db->escape($addressState).", 
				`addressPin`=".$this->db->escape($addressPin).", 
				`addressLandmark`=".$this->db->escape($addressLandmark).", 
				`shippingCost` ='$shippingcost', 
				`status` = $status
			where `addressID` = '$addressID'";
	//echo $sql;die();
	$this->bml_database->getResults($sql);
	header('Location: '.$this->agent->referrer());
  }

  public function changeStatus()
  {
    $customerNumber = $this->input->post('customerNumber');
    $status = $this->input->post('statusBtn');
    $comments = $this->input->post('comments');
	$this->adminModel->saveCustomerStatus($customerNumber, $status, $comments);
    if($status == 1)
	{
		$res = $this->adminModel->getClientDetailsByClientNumber($customerNumber);
		//print_r($res);
		$res = (array_key_exists(1, $res) ? (array_key_exists(0, $res[1])? $res[1][0]:array()): array());
		
		$this->load->library("send_email");
    $this->load->library("send_sms");
		$data['email'] = $res['emailId'];
		$data['firstName'] = $res['firstName'];
    $data['customerNumber'] = $res['customerNumber'];
    // Fetching Mobile Number with customerNumber
    //$phone=$res['mobileNumber'];
    $this->send_email->sendApprovalSuccessEmail($data);
    // Checking wheather user has mobileNumber or not
  /*   if(!empty($phone)){
    $message="You account has been successfully approved :-)";
    $this->send_sms->sendApprovalSuccessSms($phone,$message);
    } */
		
	}
    
	header('Location: '.$this->agent->referrer());
  }

  public function approve($type, $customerNumber)
  {
    $sql = "update customer set ";
    switch ($type) {
      case 'mobile':
        $sql .= "contactNumberFlag = 1";
        break;

      case 'docs':
        $sql .= "documentsFlag = 1";
        break;

      default:
        header('Location: '.$this->agent->referrer());exit();
    }
    $sql .= " where customerNumber = '$customerNumber'";
    $this->bml_database->getResults($sql);
    header('Location: '.$this->agent->referrer());exit();
  }

}
