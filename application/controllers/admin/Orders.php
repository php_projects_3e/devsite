<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!$this->session->userdata('is_admin_login'))
    {
      redirect(site_url());
    }
    $this->load->model('adminModel');
    $this->load->helper('bml_util');
  }
	
  protected function getSuborderHistoryInsertSql($suborderID)
  {
	return "insert into historysuborder(`subOrderID`, `orderSubtotal`, `orderDiscountID`, `orderDiscountAmount`, `comments`, `status`, `adminID`, `date`)
			SELECT `subOrderID`, `orderSubtotal`, `orderDiscountID`, `orderDiscountAmount`, `comments`, `status`, `adminID`, `date` FROM `tbl_suborder` WHERE subOrderID = '$suborderID';";
  }
  
  protected function getOrderProductHistoryInsertSql($suborderID)
  {
	return "insert into `historyorderproduct`(`startDate`, `endDate`, `collectDate`, `returnDate`, `subOrderId`, `itemId`, `qty`, `duration`, `rentalPrice`, `rentalTotalPrice`, `status`, `notes`, `orderProductId`)
			SELECT `startDate`, `endDate`, `collectDate`, `returnDate`, `subOrderID`, `itemId`, `qty`, `duration`, `rentalPrice`, `rentalTotalPrice`, `status`, `notes`, `orderProductId` FROM `orderproduct` WHERE `subOrderID` = '$suborderID';";
  }
  
  public function orderDetails($suborderID, $orderID)
  {
    $result = $this->adminModel->getItemsBySuborderID($suborderID);
    $result = (array_key_exists(0, $result))? $result[0]: array();
    $data['itemList'] = $result;
    $result = $this->adminModel->getOrderAddress($orderID);
    $address = [];

    $result1 = (array_key_exists(0, $result))? $result[0]: array();
    foreach ($result1 as $resultRow) {
      $address[$resultRow['shippingType']] = $resultRow;
    }
    $result2 = (array_key_exists(1, $result))? $result[1]: array();
    foreach ($result2 as $resultRow) {
      $address[$resultRow['shippingType']] = $resultRow;
    }
    $data['address'] = $address;
    echo json_encode($data);
  }

  public function booked()
  {
    $data = array("title" => "Orders", "status"=>"BOOK", "subTitle" => "Booked","sidebarCollapse" => true, 'nextStatus' => 'APPROVE');
    $locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("BOOK",$locationKey);
	

	
	$result = (array_key_exists(0, $result))? $result[0]: array();
	
	$subOrderId=$result[0]['subOrderID'];
	$result_to_quantity = $this->adminModel->getOrderDetailsBySuborderID($subOrderId);
	$data['qty'] = $result_to_quantity[2][0]['qty'];
	$data['tableRows'] = $result;
	//$data['tableRows']=$data['qty'];
	//var_dump($data);
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

 
  public function approved()
  {
    $data = array("title" => "Orders", "status"=>"APPROVE", "subTitle" => "Approved","sidebarCollapse" => true, 'nextStatus' => 'RENT');
    $locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("APPROVE",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function onrent()
  {
    $data = array("title" => "Orders", "status"=>"ONRENT", "subTitle" => "On Rent","sidebarCollapse" => true, 'nextStatus' => 'RETURNED');
	$locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("RENT",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function returned()
  {
    $data = array("title" => "Orders", "status"=>"RETURNED", "subTitle" => "Returned","sidebarCollapse" => true, 'nextStatus' => 'COMPLETE');
	$locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("RETURNED",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();
	
	$data['showLapse'] = true;
    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function completed()
  {
    $data = array("title" => "Orders", "status"=>"COMPLETE", "subTitle" => "Completed","sidebarCollapse" => true, 'nextStatus' => '');
	$locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("COMPLETE",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();
	$data['showLapse'] = true;
    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function reserved()
  {
    $data = array("title" => "Orders", "status"=>"RESERVE", "subTitle" => "Reserved","sidebarCollapse" => true, 'nextStatus' => 'APPROVE');
	$locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("RESERVE",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function waiting()
  {
    $data = array("title" => "Orders", "status"=>"WAIT", "subTitle" => "Waiting","sidebarCollapse" => true, 'nextStatus' => 'APPROVE');
	$locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("WAIT",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function cancel()
  {
    $data = array("title" => "Orders", "status"=>"CANCEL", "subTitle" => "Cancelled","sidebarCollapse" => true, 'nextStatus' => '');
	$locationKey=$this->user_session->getSessionVar('locationKey');
	
	$result = $this->adminModel->getOrderListByStatus("CANCEL",$locationKey);

    $result = (array_key_exists(0, $result))? $result[0]: array();

    $data['tableRows'] = $result;
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/orderList');
    $this->load->view('admin/footer');
  }

  public function getAddress($clientID)
  {
	$this->load->model('orderModel');
    $result = $this->orderModel->getClientAddress($clientID);
    $clientAddr = (array_key_exists(0, $result))? $result[0]: array();
    echo json_encode($clientAddr);
  }

  public function quotation()
  {
    $data = array("title" => "Orders", "subTitle" => "Quotation","sidebarCollapse" => true, 'nextStatus' => '');

    $clientResult=$this->adminModel->getClientsByStatus(1, "customerId, customerNumber, firstName, lastName, emailId");
    $clientResult = (array_key_exists(0, $clientResult))? $clientResult[0]: array();

    $clientList = array();
    foreach ($clientResult as $clientResultRow) {
      $clientList[] = array(
                          'label' => $clientResultRow['firstName'].' '.$clientResultRow['lastName'],
                          'number' => $clientResultRow['customerNumber'],
                          'email' => $clientResultRow['emailId'],
                          'id' => $clientResultRow['customerId']
                      );
    }
    $data['clientList'] = $clientList;

    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();

    $itemList = array();
    foreach ($itemResult as $itemListRow) {
      $itemList[] = array(
                          'label' => str_replace('/', "'", $itemListRow['itemName'])
                      );
    }
    $data['itemList'] = $itemList;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/quotation');
    $this->load->view('admin/footer');
  }
  
  public function newOrder()
  {
    $data = array("title" => "Orders", "subTitle" => "New","sidebarCollapse" => true, 'nextStatus' => '');

    $clientResult=$this->adminModel->getClientsByStatus(1, "customerId, customerNumber, firstName, lastName, emailId");
    $clientResult = (array_key_exists(0, $clientResult))? $clientResult[0]: array();

    $clientList = array();
    foreach ($clientResult as $clientResultRow) {
      $clientList[] = array(
                          'label' => $clientResultRow['firstName'].' '.$clientResultRow['lastName'],
                          'number' => $clientResultRow['customerNumber'],
                          'email' => $clientResultRow['emailId'],
                          'id' => $clientResultRow['customerId']
                      );
    }
    $data['clientList'] = $clientList;

    $itemResult=$this->adminModel->getProductDetailsByStatusActive();
    $itemResult = (array_key_exists(0, $itemResult))? $itemResult[0]: array();

    $itemList = array();
    foreach ($itemResult as $itemListRow) {
      $itemList[] = array(
                          'label' => str_replace('/', "'", $itemListRow['itemName'])
                      );
    }
    $data['itemList'] = $itemList;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/newOrder');
    $this->load->view('admin/footer');
  }

  public function pickOrderDatesUI($itemName)
  {
    $itemName = bml_urldecode($itemName);

    //get product details
    $this->load->model('productModel');
    $productDetailsResult = $this->productModel->getProductDetails($itemName);
    $productDetails = (array_key_exists(0, $productDetailsResult)) ? $productDetailsResult[0]: array();//get first resultset
    $productDetails = (array_key_exists(0, $productDetails)) ? $productDetails[0]: array();//get first row

    $resultSet = $this->productModel->getPriceByProductIDAndSeasonID($productDetails['itemId'], $productDetails['seasonID']);
    $productDetails['priceList'] = (array_key_exists(0, $resultSet)) ? $resultSet[0]: array();
    $priceListArr = [];
    foreach ($productDetails['priceList'] as $priceList) {
      $priceListArr[$priceList['days']] = $priceList['price'];
    }
    $data['priceListArr'] = $priceListArr;
    $data['productDetails'] = $productDetails;

    $this->load->model('orderModel');
    $availableCountRes = $this->orderModel->getAvailableQtyByItemId($productDetails['itemId']);
    $availableQty = (array_key_exists(0, $availableCountRes)) ? $availableCountRes[0]: array();
   
    $totalQty = (array_key_exists(1, $availableCountRes)) ? $availableCountRes[1][0]['totalQty']: array();
  
    $unAvailableQtyArr = array();
    foreach ($availableQty as $availableQtyRow) {
        $availableQtyRow['dates'];
        if($availableQtyRow['availableQty'] <= 0){
            $unAvailableQtyArr[] = date_format(date_create($availableQtyRow['dates']), "Y-m-d");
        }
    }
    $data['unAvailableDates'] = $unAvailableQtyArr;

    $holidayListRes = $this->orderModel->getHolidayList();
    $holidayList = (array_key_exists(0, $holidayListRes)) ? $holidayListRes[0]: array();
    $holidayListArr = array();
    foreach ($holidayList as $holidayListRow) {
        $holidayListArr[] = $holidayListRow['holidayDate'];
    }
    $data['holidayList'] = $holidayListArr;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/pickOrderDatesUI');
    $this->load->view('admin/footer');
  }

  public function quotationConfirm()
  {
    $data = array("title" => "Orders", "subTitle" => 'quotation confirm', "sidebarCollapse" => true);

    $valueJson = $this->input->post('valueJson');
    $client = $this->input->post('client');
    $clientNumber = trim(explode('|', $client)[0]);
    $pickAddr = $this->input->post('pickAddr');
    $returnAddr = $this->input->post('returnAddr');

	$data['name'] = $this->input->post('name');
    $data['address'] = $this->input->post('address');
	
    $data['client'] = $client;
    $data['clientNumber'] = $clientNumber;
    $data['pickAddrID'] = $pickAddr;
    $data['returnAddrID'] = $returnAddr;

    $this->load->model('orderModel');
    $pickAddr = $this->orderModel->getClientAddressbyID($pickAddr);
    $pickAddr = (array_key_exists(0, $pickAddr) ? $pickAddr[0][0] : array());
    $data['pickAddr'] =$pickAddr['addressLine1'].', '.$pickAddr['addressLine2'].', '.$pickAddr['addressCity'].', '.$pickAddr['addressState'].', '.', '.$pickAddr['addressPin'].', '.$pickAddr['addressLandmark'];
    $returnAddr = $this->orderModel->getClientAddressbyID($returnAddr);
    $returnAddr = (array_key_exists(0, $returnAddr) ? $returnAddr[0][0] : array());
    $data['returnAddr'] = $returnAddr['addressLine1'].', '.$returnAddr['addressLine2'].', '.$returnAddr['addressCity'].', '.$returnAddr['addressState'].', '.', '.$returnAddr['addressPin'].', '.$returnAddr['addressLandmark'];

    $orderDetails = [];
    foreach (json_decode($valueJson) as $valueRow){
      $avail = $this->checkAvailProduct($valueRow[0], date_create_from_format('d/M/Y', $valueRow[2]), date_create_from_format('d/M/Y', $valueRow[3]));
      $orderDetails[] = array('itemId' => $valueRow[0], 'itemName' => $valueRow[1], 'startDate' => $valueRow[2], 'endDate' => $valueRow[3], 'days' => $valueRow[4], 'price' => $valueRow[5], 'avail' => $avail);
    }
    $data['orderDetails'] = $orderDetails;

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/quotationConfirm');
    $this->load->view('admin/footer');

  }

  public function insertNewOrderConfirm()
  {
    $data = array("title" => "Orders", "subTitle" => 'new order confirm', "sidebarCollapse" => true);

    $valueJson = $this->input->post('valueJson');
    $client = $this->input->post('client');
    $clientNumber = trim(explode('|', $client)[0]);
    $pickAddr = $this->input->post('pickAddr');
    $returnAddr = $this->input->post('returnAddr');

    $data['client'] = $client;
    $data['clientNumber'] = $clientNumber;
    $data['pickAddrID'] = $pickAddr;
    $data['returnAddrID'] = $returnAddr;

    $this->load->model('orderModel');
    $pickAddr = $this->orderModel->getClientAddressbyID($pickAddr);
    $pickAddr = (array_key_exists(0, $pickAddr) ? $pickAddr[0][0] : array());
    $data['pickAddr'] =$pickAddr['addressLine1'].', '.$pickAddr['addressLine2'].', '.$pickAddr['addressCity'].', '.$pickAddr['addressState'].', '.', '.$pickAddr['addressPin'].', '.$pickAddr['addressLandmark'];
    $returnAddr = $this->orderModel->getClientAddressbyID($returnAddr);
    $returnAddr = (array_key_exists(0, $returnAddr) ? $returnAddr[0][0] : array());
    $data['returnAddr'] = $returnAddr['addressLine1'].', '.$returnAddr['addressLine2'].', '.$returnAddr['addressCity'].', '.$returnAddr['addressState'].', '.', '.$returnAddr['addressPin'].', '.$returnAddr['addressLandmark'];

    $orderDetails = [];
    foreach (json_decode($valueJson) as $valueRow){
      $avail = $this->checkAvailProduct($valueRow[0], date_create_from_format('d/M/Y', $valueRow[2]), date_create_from_format('d/M/Y', $valueRow[3]));
      $orderDetails[] = array('itemId' => $valueRow[0], 'itemName' => $valueRow[1], 'startDate' => $valueRow[2], 'endDate' => $valueRow[3], 'days' => $valueRow[4], 'price' => $valueRow[5], 'avail' => $avail);
    }
    $data['orderDetails'] = $orderDetails;

	$sql = "SELECT sum(`points`) as 'totalpoints', `pointsType` FROM `points_vallet` WHERE `customerId` = (SELECT `customerId` FROM `customer` WHERE `customerNumber` = '$clientNumber') group by `pointsType`;";
    $points_vallet_res = $this->bml_database->getResults($sql);
    $points_vallet_summary_arr = (array_key_exists(0, $points_vallet_res)) ? $points_vallet_res[0]: array();//get first resultset
    $points_vallet_summary = array();
	foreach($points_vallet_summary_arr as $row)
	{
		$points_vallet_summary[$row['pointsType']] = $row['totalpoints'];
	}
	$data['points_vallet_summary'] = $points_vallet_summary;
	
	$sql = "SELECT `promocode` FROM `discount` WHERE `discountId` = (
					SELECT `discountID` FROM `points_vallet` WHERE `discountID` not in (
						SELECT `discountID` FROM `ordertb` WHERE `customerId` = (SELECT `customerId` FROM `customer` WHERE `customerNumber` = '$clientNumber')  and discountID > 0) 
					and `customerId` = (SELECT `customerId` FROM `customer` WHERE `customerNumber` = '$clientNumber') and discountID > 0) and discountType = 'FIXED';";
    $promocode_res = $this->bml_database->getResults($sql);
    $promocode_res = (array_key_exists(1, $promocode_res)) ? $promocode_res[1]: array();
	$promocode_res = (array_key_exists(0, $promocode_res)) ? $promocode_res[0]: array();
	$data['promocode'] = (array_key_exists('promocode', $promocode_res)? $promocode_res['promocode']: null);
	
	if($data['promocode'] == "")
	{
		$earnedCost = (array_key_exists('EARNED', $points_vallet_summary)? $points_vallet_summary['EARNED']: "0");
		$spentCost = (array_key_exists('SPENT', $points_vallet_summary)? $points_vallet_summary['SPENT']: "0");
			
		if($earnedCost - $spentCost > 250)
		{
			$sql = "call sp_generate_discount_number('".$this->user_session->getSessionVar('customerId')."')";
			$res = $this->bml_database->getResults($sql);
			$promocode = ( (is_array($res) && array_key_exists(0, $res)? 
									(is_array($res) && array_key_exists(0, $res[0])? 
										(is_array($res) && array_key_exists('discountCode', $res[0][0])? 
											$res[0][0]['discountCode']: '')
										: '')
									: '') );
			if($promocode != '')
			{
				$sql = "INSERT INTO `discount` (`discount`, `discountType`, `discountCount`, `promocode`, `customerIds`, `status`)
					VALUES ('250', 'FIXED', 1, '$promocode', '".$this->user_session->getSessionVar('customerId')."', 1);";
				$sql .= "SET @discountId = LAST_INSERT_ID();";
				$sql .= "insert into `points_vallet`(`customerId`, `points`, `pointsType`, `discountID`)
						values('".$this->user_session->getSessionVar('customerId')."', '250', 'SPENT', @discountId);";
				$this->bml_database->getResults($sql);
			}
		}
	}
    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/newOrderConfirm');
    $this->load->view('admin/footer');

  }

  public function generateQuotation()
  {
	  $data = array();
    
	  $valueJson = $this->input->post('valueJson');
	  $clientNumber = $this->input->post('clientNumber');
	  $pickAddr = $this->input->post('pickAddrID');
	  $returnAddr = $this->input->post('returnAddrID');
	  $data['name'] = $this->input->post('name');
	  $data['address'] = $this->input->post('address');
	  
	  $orderDetails = [];
	  foreach (json_decode($valueJson) as $valueRow){
		if($valueRow[6] == 0) continue;
		$avail = $this->checkAvailProduct($valueRow[0], date_create_from_format('d/M/Y', $valueRow[2]), date_create_from_format('d/M/Y', $valueRow[3]));
		$orderDetails[] = array('id' => $valueRow[0], 'itemName' => $valueRow[1], 'startDate' => $valueRow[2], 'endDate' => $valueRow[3], 'days' => $valueRow[4], 'price' => $valueRow[5], 'avail' => $avail);
	  }

	  $result = $this->placeOrder($clientNumber, $pickAddr, $returnAddr, $orderDetails, true);

	  if($result['status'])
	  {
		//$result['orderID'] = '9503';
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getOrderDetails($result['orderID']);
		$data = array_merge($data, $orderDetails[0][0]);

		$subOrderItems = [];
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}

		$data['subOrderItems'] = $subOrderItems;
		$sql = "SELECT `customerId`, `mobileNumber`, `landlineNumber`, `officeNumber`, `customerNumber`, `firstName`, `lastName` FROM `customer` WHERE  `customerNumber` = '".$clientNumber."'";
		$res = $this->bml_database->getResults($sql);
		$data['custDetails'] = $res[0][0];
		
		$data['name'] = isset($data['name']) ? $data['name'] : $data['custDetails']['firstName']." ".$data['custDetails']['lastName'];
		if(trim($data['address']) == "")
		{
			$sql = "SELECT * FROM `customer_address` WHERE `customerId` = '".$data['custDetails']['customerId']."' and `addressType` = 'HOME'";
			$res = $this->bml_database->getResults($sql);
			if(isset($res[0][0])) {
				$pickAddr = $res[0][0];
				$data['address'] = $pickAddr['addressLine1'].', '.$pickAddr['addressLine2'].', '.$pickAddr['addressCity'].', '.$pickAddr['addressState'].', '.', '.$pickAddr['addressPin'].', '.$pickAddr['addressLandmark'];
			}
		}
		
		$html=$this->load->view('admin/pdfQuotation', $data, true);
 
        //this the the PDF filename that user will get to download
        $pdfFilePath = "EST"."_".strtotime("now")."_".$data['custDetails']['customerNumber'].".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D");  
						
	  }
	  else {
		echo $result['message'];
	  }
  }

  public function insertNewOrder($orderID = '')
  {
    $data = array("title" => "Orders", "subTitle" => 'new order confirm', "sidebarCollapse" => true);
    if($orderID == '')
    {
      $valueJson = $this->input->post('valueJson');
      $clientNumber = $this->input->post('clientNumber');
      $pickAddr = $this->input->post('pickAddrID');
      $returnAddr = $this->input->post('returnAddrID');
      $discountCode = trim($this->input->post('coupon'));
	  $discountAmt = 0;$discountType = "FIXED";
	  $discountId = -1;
	  $error = "";
	  if($discountCode != "")
	  {
		$this->load->model('orderModel');
		$result = $this->orderModel->getDiscountDetails($discountCode);
		$discountCount = array_key_exists(1, $result)? (array_key_exists(0, $result[1])? (array_key_exists('count', $result[1][0])? $result[1][0]['count']: 0 ) : 0 ): 0;
		$discountDetails = array_key_exists(0, $result)? (array_key_exists(0, $result[0])? $result[0][0] : array() ): array();
		if(empty($discountDetails))
		{
			$error = "invalid code";
		}
		else{
			if($discountDetails['customerIds'] != "0")
			{
				$sql = "select `customerId` from customer where customerNumber = '$clientNumber'";
				$res = $this->bml_database->getResults($sql);
				$custID = ($res[0][0]['customerId']) ? $res[0][0]['customerId'] : -1;
				if(!  in_array(
                  $custID,
                  explode(",",$discountDetails['customerIds'])
                )
				) {
					$error = "Not Applicable";
				}
				else if($discountDetails['discountCount'] > 0
				  && $discountCount >= $discountDetails['discountCount']){
					$error = "Not Applicable";
				}
				else {
					$discountAmt = $discountDetails['discount'];
					$discountType = $discountDetails['discountType'];
					$discountId = $discountDetails['discountId'];
		  
				}
			}
			else {
				$discountAmt = $discountDetails['discount'];
				$discountType = $discountDetails['discountType'];
				$discountId = $discountDetails['discountId'];
			}
		  }
	  }
      $orderDetails = [];
      foreach (json_decode($valueJson) as $valueRow){
        if($valueRow[6] == 0) continue;
        $avail = $this->checkAvailProduct($valueRow[0], date_create_from_format('d/M/Y', $valueRow[2]), date_create_from_format('d/M/Y', $valueRow[3]));
        $orderDetails[] = array('id' => $valueRow[0], 'itemName' => $valueRow[1], 'startDate' => $valueRow[2], 'endDate' => $valueRow[3], 'days' => $valueRow[4], 'price' => $valueRow[5], 'avail' => $avail);
      }

      $result = $this->placeOrder($clientNumber, $pickAddr, $returnAddr, $orderDetails, false, $discountId, $discountAmt, $discountType);

      if($result['status'])
      {
        redirect(admin_url('orders/insertNewOrder/'.$result['orderID']));
      }
      else {
        echo $result['message'];
      }
    }
    else
    {
      $this->load->model('orderModel');
      $orderDetails = $this->orderModel->getOrderDetails($orderID);
      $data = array_merge($data, $orderDetails[0][0]);

      $subOrderItems = [];
      foreach ($orderDetails[1] as $orderItem) {
        $subOrderItems[$orderItem['orderNumber']][] = $orderItem;
      }

      $data['subOrderItems'] = $subOrderItems;
      $this->load->view('admin/head',$data);
      $this->load->view('admin/header');
      $this->load->view('admin/newOrderSummary');
      $this->load->view('admin/footer');
    }
  }

  protected function placeOrder($clientNumber, $pickupAddrID, $returnAddrID, $orderItems, $isQuotation = false, $discountId = -1, $discountAmt = 0, $discountType = "FIXED")
  {
	//start manual sql transaction
    $this->db->trans_begin();
	$status = ($isQuotation)?10:9;
    //insert new order
    $sql = "INSERT INTO `ordertb`(`total`, `discountID`, `dicountAmount`, `status`, `customerId`, `deliveryAddress`, `pickupAddress`, `orderDate`, `adminID`)"
            ." select 0, 0, 0, $status, `customerId`, '".$pickupAddrID."', '".$returnAddrID."', now(), '".$this->user_session->getSessionVar('customerId')."' from customer where customerNumber = '$clientNumber'";
    $this->bml_database->getResults($sql);
    $orderID = $this->db->insert_id();
    $orderTotal = 0;$orderDiscountAmount = 0;

    //group products by start and end date
    $orders = [];
    foreach ($orderItems as $items) {
      if(empty($orders))
      {
        $orders[] = array(
                      "start" => $items['startDate'],
                      "end" => $items['endDate'],
                      "waiting" => 0,
                      "items" => array($items)
                    );
        continue;
      }
      $insert = false;
      for ($i=0; $i < count($orders); $i++) {
        if($orders[$i]['start'] == $items['startDate']
            && $orders[$i]['end'] == $items['endDate']){
          $orders[$i]['items'][] = $items;
          $insert = true;
        }
      }
      if(!$insert)
      {
        $orders[] = array(
                      "start" => $items['startDate'],
                      "end" => $items['endDate'],
                      "waiting" => 0,
                      "items" => array($items)
                    );
      }
    }

    //insert each product group (suborder)
    foreach ($orders as $orderRow) {

      //generate unique orderID
      $orderNumber = '';
      
	  //insert suborder to db
	  $sql = "INSERT INTO `tbl_suborder`(`orderID`, orderDiscountID, status) VALUES ($orderID, '0', '$status')";
      $this->bml_database->getResults($sql);
      $subOrderID = $this->db->insert_id();
		$orderNumber = "OBML".sprintf("%06d", $subOrderID);
      //insert each product of this suborder/subgroup
      $subTotal = 0; $discountAmount = 0;$orderStatus=1;
      foreach ($orderRow['items'] as $items) {
        $subTotal += $items['price'];
		if($discountType != "FIXED")
		{
			$totalPrice = $items['price'];
			$totalPriceBeforeTax = $totalPrice / 1.145; // 14.5% VAT
			$discountAmount += ($totalPriceBeforeTax) * $discount / 100;
		}
		
        $collectDate = date_create_from_format('d/M/Y', $items['startDate']);
        $returnDate = date_create_from_format('d/M/Y', $items['endDate']);
        $date1 = date_create_from_format('d/M/Y', $items['startDate']);
        $date2 = date_create_from_format('d/M/Y', $items['endDate']);

        $collectDate->sub(new DateInterval('P1D'));
        $returnDate->add(new DateInterval('P1D'));

        //check if there is any order one day before start date and one day after end date
        $sql = "SELECT  count(1) as 'numOfOrders'
                FROM `orderproduct` a
                where a.`itemId` = '".$items['id']."'
                and a.`startDate` <= '".$collectDate->format('Y-m-d')."'
                and a.`endDate` >= '".$collectDate->format('Y-m-d')."';\n";
        $sql .= "SELECT  count(1) as 'numOfOrders'
                FROM `orderproduct` a
                where a.`itemId` = ".$items['id']."
                and a.`startDate` <= '".$returnDate->format('Y-m-d')."'
                and a.`endDate` >= '".$returnDate->format('Y-m-d')."';\n";
        $sql .= "SELECT `numStock` FROM `itemmaster` WHERE `itemId` = '".$items['id']."';";
        $result = $this->bml_database->getResults($sql);
        $totalQty = $result[2][0]['numStock'];

        if($collectDate->format('N') != 6 && $result[0][0]['numOfOrders'] >= $totalQty)
        {
          $collectDate->add(new DateInterval('P1D'));
        }
		else if($collectDate->format('N') == 7)
        {
          $collectDate->add(new DateInterval('P1D'));
        }
        if($returnDate->format('N') == 7 || $result[1][0]['numOfOrders'] >= $totalQty)
        {
          $returnDate->sub(new DateInterval('P1D'));
        }

        $orderStatus = $status ;
        
        $get_location="SELECT `locationKey` FROM customer WHERE `customerNumber`='$clientNumber'";
       $locationKey= $this->bml_database->getResults($get_location);
        $location= $locationKey[0][0]['locationKey'];
         //var_dump($locationKey[0][0]['locationKey']);
        $sql = "INSERT INTO `orderproduct`(`startDate`, `endDate`, `collectDate`, `returnDate`, `subOrderID`, `itemId`, `duration`, `rentalTotalPrice`, `status`,`qty`,`locationKey`)
                VALUES ('".$date1->format('Y-m-d')."', '".$date2->format('Y-m-d')."', '".$collectDate->format('Y-m-d')."', '".$returnDate->format('Y-m-d')."', ".$subOrderID.", ".$items['id'].", ".$items['days'].", ".$items['price'].", '".$orderStatus ."', 1,'".$location."')";
        $this->bml_database->getResults($sql);
      }

      //update suborderTotal
      $sql = "UPDATE `tbl_suborder` SET `orderNumber` = '".$orderNumber."', `orderSubtotal`=$subTotal, `orderDiscountAmount` = $discountAmount, `status` = $orderStatus WHERE `subOrderID` = $subOrderID";
      $this->bml_database->getResults($sql);
      $orderTotal += $subTotal;
	  $orderDiscountAmount += $discountAmount;
    }

    $sql = "UPDATE `ordertb` SET `total`=$orderTotal, `dicountAmount`='".$orderDiscountAmount."' WHERE `orderId` = $orderID; ";
    //add shipping Details
	$sql .= "insert into `shipping_tb` (`orderID`, `addressID`, `ship_type`, `ship_flg`) values ( '".$orderID."', '".$pickupAddrID."', 'P', 0);";
	$sql .= "insert into `shipping_tb` (`orderID`, `addressID`, `ship_type`, `ship_flg`) values ( '".$orderID."', '".$returnAddrID."', 'R', 0);";
	$this->bml_database->getResults($sql);
	
    //check transaction status
    if ($this->db->trans_status() === FALSE)
    {
        $this->db->trans_rollback();
        return array("status" => false, "message" => $this->db->_error_message());
    }
    else
    {
        $this->db->trans_commit();
        //$this->db->trans_rollback();
		if(!$isQuotation)
		{
		//send Email
		  $this->load->model('orderModel');
		  $orderDetails = $this->orderModel->getOrderDetails($orderID);
		  
		  $data = $orderDetails[0][0];

		  $subOrderItems = [];
		  foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		  }
		  
			$addressRet = $this->orderModel->getClientAddressbyID($pickupAddrID);
			$data['pickupAddr'] = $addressRet[0][0];
			$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

			$addressRet = $this->orderModel->getClientAddressbyID($returnAddrID);
			$data['returnAddr'] = $addressRet[0][0];
			$data['shippingCost'] += $data['returnAddr']['shippingCost'];
		  //print_r($data);
			$this->load->library("send_email");
			
			$eData['total'] = $data['total'];
			$eData['shippingCost'] = $data['shippingCost'];
			
			$sql= "SELECT `customerId`,`customerNumber`, `firstName`, `emailId` FROM `customer` WHERE `customerNumber`='".$clientNumber."'";
			$result = $this->bml_database->getResults($sql);
			$result = $result[0][0];
			
			$eData['email'] = $result['emailId'];
			$eData['firstName'] = $result['firstName'];
			$eData['customerNumber'] = $result['customerNumber'];
			$eData['subOrderItems'] = $subOrderItems;

			$sql = "SELECT sum(`points`) as 'totalpoints', `pointsType` FROM `points_vallet` WHERE `customerId` = '".$result['customerId']."' group by `pointsType`;";
			$points_vallet_res = $this->bml_database->getResults($sql);
			$points_vallet_summary_arr = (array_key_exists(0, $points_vallet_res)) ? $points_vallet_res[0]: array();//get first resultset
			$points_vallet_summary = array();
			foreach($points_vallet_summary_arr as $row)
			{
				$points_vallet_summary[$row['pointsType']] = $row['totalpoints'];
			}
			$eData['points_vallet_summary'] = $points_vallet_summary;
		
			$this->send_email->sendOrderSummaryEmail($eData);
		}		
        return array("status" => true, "orderID" => $orderID);
    }
  }

  public function editPrice()
  {
    $orderProductID = $this->input->post('orderProductID');
    $price = $this->input->post('price');
    $rentalPrice = $this->input->post('rentalPrice');
    $this->load->model('adminModel');
    $this->adminModel->changeOrderProductPrice($orderProductID, $price, $rentalPrice);
  }

  public function editOrder($subOrderId)
  {
    $this->load->model('adminModel');
    $data = array("title" => "Orders View", "subTitle" => '', "sidebarCollapse" => true);

    $result = $this->adminModel->getOrderDetailsBySuborderID($subOrderId);
	
    $data['orderDetails'] = $result[0][0];
    $orderAddress = array();
	if(array_key_exists(1, $result))
	{
		$orderAddressRes = $result[1];
    
		foreach ($orderAddressRes as $orderAddressRow) {
		  $orderAddress[$orderAddressRow['shippingType']] = $orderAddressRow;
		}
	}
	 
    $data['subTitle'] = "#".$data['orderDetails']['orderNumber'];
    $data['orderAddress'] = $orderAddress;
    $data['orderItems'] = $result[2];

    //get dates from orderItem
    $item = $data['orderItems'][0];
    $data['startDate'] = $item['startDate'];
    $data['collectDate'] = $item['collectDate'];
    $data['endDate'] = $item['endDate'];
    $data['returnDate'] = $item['returnDate'];

    $this->load->view('admin/head',$data);
    $this->load->view('admin/header');
    $this->load->view('admin/editOrder');
    $this->load->view('admin/footer');
  }

  public function approve($subOrderId)
  {
	$sql = $this->getSuborderHistoryInsertSql($subOrderId);
	$sql .= $this->getOrderProductHistoryInsertSql($subOrderId);
    $sql .= "SELECT `orderStatusID` INTO @approveStatusID FROM `tbl_order_status` WHERE `orderStatsCode` = 'APPROVE';
            UPDATE `tbl_suborder` SET `status`= @approveStatusID, `date` = now(), `adminID` = '".$this->user_session->getSessionVar('customerId')."' WHERE `subOrderID` = '$subOrderId';
            UPDATE `orderproduct` SET `status`=@approveStatusID WHERE `subOrderID` = '$subOrderId'";

	$this->bml_database->getResults($sql);
	// minus the points
	/*$result = $this->adminModel->getDiscountPoints($this->user_session->getSessionVar('customerId'));
	$discount = $result[0][0]['discount'];
	$result=$this->adminModel->updateDiscountPoints($this->user_session->getSessionVar('customerId'),$discount);
	*/
	$this->sendApprovalEmail($subOrderId);
    header('Location: '.$this->agent->referrer());
  }

  
  public function sendCancelEmail($subOrderId)
	{
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getSubOrderDetails($subOrderId);
		//print_r($orderDetails);die();
		$data = $orderDetails[0][0];

		$subOrderItems = [];
		
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}
		$data['subOrderItems'] = $subOrderItems;
		
		$sql = "SELECT `deliveryAddress`, `pickupAddress` FROM `ordertb` WHERE `orderId` = (SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '$subOrderId')";
		$result = $this->bml_database->getResults($sql);
		$result = $result[0][0];
		$pickupAddrID = $result['deliveryAddress'];
		$returnAddrID = $result['pickupAddress'];
		
		$addressRet = $this->orderModel->getClientAddressbyID($pickupAddrID);
		$data['pickupAddr'] = $addressRet[0][0];
		$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

		$addressRet = $this->orderModel->getClientAddressbyID($returnAddrID);
		$data['returnAddr'] = $addressRet[0][0];
		$data['shippingCost'] += $data['returnAddr']['shippingCost'];
			
	  //print_r($data);
		$this->load->library("send_email");
		
		$eData['total'] = $data['total'];
		$eData['shipCost'] = $data['shippingCost'];
		$eData['subOrderItems'] = $subOrderItems;
		$this->send_email->sendOrderCancelEmail($eData, $subOrderId);
		
	}
	
	
	public function sendApprovalEmail($subOrderId)
	{
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getSubOrderDetails($subOrderId);

		$data = $orderDetails[0][0];

		$subOrderItems = [];
		
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}
		$data['subOrderItems'] = $subOrderItems;
		
		$sql = "SELECT `deliveryAddress`, `pickupAddress` FROM `ordertb` WHERE `orderId` = (SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '$subOrderId')";
		$result = $this->bml_database->getResults($sql);
		$result = $result[0][0];
		$pickupAddrID = $result['deliveryAddress'];
		$returnAddrID = $result['pickupAddress'];
		
		$addressRet = $this->orderModel->getClientAddressbyID($pickupAddrID);
		$data['pickupAddr'] = $addressRet[0][0];
		$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

		$addressRet = $this->orderModel->getClientAddressbyID($returnAddrID);
		$data['returnAddr'] = $addressRet[0][0];
		$data['shippingCost'] += $data['returnAddr']['shippingCost'];
			
	  //print_r($data);
		$this->load->library("send_email");
		
		$eData['total'] = $data['total'];
		$eData['shipCost'] = $data['shippingCost'];
		$eData['subOrderItems'] = $subOrderItems;
		$this->send_email->sendOrderApprovalEmail($eData, $subOrderId);
		
	}
	public function sendApprovalSms($subOrderId){
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getSubOrderDetails($subOrderId);

		$data = $orderDetails[0][0];

		$subOrderItems = [];
		
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}
		$data['subOrderItems'] = $subOrderItems;
		
		$sql = "SELECT `deliveryAddress`, `pickupAddress` FROM `ordertb` WHERE `orderId` = (SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '$subOrderId')";
		$result = $this->bml_database->getResults($sql);
		$result = $result[0][0];
		$pickupAddrID = $result['deliveryAddress'];
		$returnAddrID = $result['pickupAddress'];
		
		$addressRet = $this->orderModel->getClientAddressbyID($pickupAddrID);
		$data['pickupAddr'] = $addressRet[0][0];
		$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

		$addressRet = $this->orderModel->getClientAddressbyID($returnAddrID);
		$data['returnAddr'] = $addressRet[0][0];
		$data['shippingCost'] += $data['returnAddr']['shippingCost'];

    	$this->load->library("send_sms");
		
		$eData['total'] = $data['total'];
		$eData['shipCost'] = $data['shippingCost'];
		$eData['subOrderItems'] = $subOrderItems;
		$this->send_sms->sendOrderApprovalSms($eData, $subOrderId);
  
	}
	
	public function sendCompleteEmail($subOrderId)
	{
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getSubOrderDetails($subOrderId);

		$data = $orderDetails[0][0];

		$subOrderItems = [];
		
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}
		$data['subOrderItems'] = $subOrderItems;
		
		$sql = "SELECT `deliveryAddress`, `pickupAddress` FROM `ordertb` WHERE `orderId` = (SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '$subOrderId')";
		$result = $this->bml_database->getResults($sql);
		$result = $result[0][0];
		$pickupAddrID = $result['deliveryAddress'];
		$returnAddrID = $result['pickupAddress'];
		
		$addressRet = $this->orderModel->getClientAddressbyID($pickupAddrID);
		$data['pickupAddr'] = $addressRet[0][0];
		$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

		$addressRet = $this->orderModel->getClientAddressbyID($returnAddrID);
		$data['returnAddr'] = $addressRet[0][0];
		$data['shippingCost'] += $data['returnAddr']['shippingCost'];
			
	  //print_r($data);
		$this->load->library("send_email");
		
		$eData['total'] = $data['total'];
		$eData['shipCost'] = $data['shippingCost'];
		$eData['subOrderItems'] = $subOrderItems;
		$this->send_email->sendOrderCompleteEmail($eData, $subOrderId);
		
	}

	public function sendCompleteSms($subOrderId){
		$this->load->model('orderModel');
		$orderDetails = $this->orderModel->getSubOrderDetails($subOrderId);

		$data = $orderDetails[0][0];

		$subOrderItems = [];
		
		foreach ($orderDetails[1] as $orderItem) {
			$subOrderItems[$orderItem['orderNumber']][] = $orderItem;
		}
		$data['subOrderItems'] = $subOrderItems;
		
		$sql = "SELECT `deliveryAddress`, `pickupAddress` FROM `ordertb` WHERE `orderId` = (SELECT `orderID` FROM `tbl_suborder` WHERE `subOrderID` = '$subOrderId')";
		$result = $this->bml_database->getResults($sql);
		$result = $result[0][0];
		$pickupAddrID = $result['deliveryAddress'];
		$returnAddrID = $result['pickupAddress'];
		
		$addressRet = $this->orderModel->getClientAddressbyID($pickupAddrID);
		$data['pickupAddr'] = $addressRet[0][0];
		$data['shippingCost'] = $data['pickupAddr']['shippingCost'];

		$addressRet = $this->orderModel->getClientAddressbyID($returnAddrID);
		$data['returnAddr'] = $addressRet[0][0];
		$data['shippingCost'] += $data['returnAddr']['shippingCost'];
			
	  //print_r($data);
		$this->load->library("send_sms");
		
		$eData['total'] = $data['total'];
		$eData['shipCost'] = $data['shippingCost'];
		$eData['subOrderItems'] = $subOrderItems;
		$this->send_sms->sendOrderCompleteSms($eData, $subOrderId);
		
	}
	
  public function bulkAction($statusCode){
    foreach (explode("\n",trim($this->input->post('tableData'))) as $suborderID) {
      $this->_updateStatusByCode(trim($suborderID), trim($statusCode));
    }
    echo "success";
  }

  public function updateOrderDates()
  {
	$startDate = $this->input->post('startDate');
	$endDate = $this->input->post('endDate');
	$subOrderID = $this->input->post('subOrderID');
	
	//get order Items
	$sql = "SELECT a.orderProductId, a.`itemId`, a.startDate, a.endDate, b.itemName
			FROM `orderproduct` a, itemmaster b 
			WHERE a.itemId = b.itemId
			and a.`subOrderID` = '$subOrderID'";
			
	$res = $this->bml_database->getResults($sql);
	if(!isset($res[0]))
	{
		$res = array("status"=>"error", "message"=>"No Products");
		echo json_encode($res);die();
	}
	$productArr = $res[0];
	
	$message = '';
	
	$today =  new DateTime();
	$collectDate = new DateTime($startDate);
	$returnDate = new DateTime($endDate);
	$date1 = new DateTime($startDate);
	$date2 = new DateTime($endDate);
	$interval = $date1->diff($date2);
	if($date1 > $date2)
	{
		$res = array("status"=>"error", "message"=>'Check Dates');
		echo json_encode($res);
		return;
	}
	$duration = $interval->days+1;

	$collectDate->sub(new DateInterval('P1D'));
	$returnDate->add(new DateInterval('P1D'));
	
	//holiday on both collectDate and startDate
	$sql = "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$collectDate->format('Y-m-d')."'  or `holidayDate` = '".$date1->format('Y-m-d')."'";
    $result = $this->bml_database->getResults($sql);
    if(isset($result[0][0]['numRow']) && $result[0][0]['numRow'] >= 2) {
		$message = "Holiday on '".$collectDate->format('Y-m-d')."' and '".$date1->format('Y-m-d')."'";
	}
  
	//holiday on both returnDate and endDate
    $sql = "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$returnDate->format('Y-m-d')."'  or `holidayDate` = '".$date2->format('Y-m-d')."'";
    $result = $this->bml_database->getResults($sql);
    if(isset($result[0][0]['numRow']) && $result[0][0]['numRow'] == 2){
		$message = "Holiday on '".$date2->format('Y-m-d')."' and '".$returnDate->format('Y-m-d')."'";
	}
   
   if($message != '')
   {
	$res = array("status"=>"error", "message"=>$message);
	echo json_encode($res);
	return;
   }
   
   $adjCollectDates = $adjReturnDates = true;
	$sql = "select 1; select 1;select 1;SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$collectDate->format('Y-m-d')."';";
	$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$returnDate->format('Y-m-d')."';";
	$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$date1->format('Y-m-d')."';";
	$sql .= "SELECT count(1) as numRow FROM `holidaylist` WHERE `holidayDate` = '".$date2->format('Y-m-d')."';";
	$result = $this->bml_database->getResults($sql);
	
	//if holiday on collectDate, move to next date
	if(isset($result[3][0]['numRow']) && $result[3][0]['numRow'] == 1)
	{
		$collectDate->add(new DateInterval('P1D'));
		$adjCollectDates = false;
	}
	//if holiday on startDate or collectDate on Saturday, can't move to next date
	else if((isset($result[5][0]['numRow']) && $result[5][0]['numRow'] == 1) || $collectDate->format('N') == 6)
	{
		$adjCollectDates = false;
	}
	//if collectDate on sunday, move to monday
	else if($collectDate->format('N') == 7)
	{
		$adjCollectDates = false;
	}
	//if startDate is today, move collectDate to today
	else if($date1 == $today)
	{
	  $collectDate->add(new DateInterval('P1D'));$adjCollectDates = false;
	}
	//if holiday on returnDate, move to prev date
	if(isset($result[4][0]['numRow']) && $result[4][0]['numRow'] == 1)
	{
		$returnDate->sub(new DateInterval('P1D'));$adjReturnDates = false;
	}
	//if holiday on endDate, can't move to prev date
	else if((isset($result[6][0]['numRow']) && $result[6][0]['numRow'] == 1))
	{
		$adjReturnDates = false;
	}
	//if returnDate is sunday, move to saturday 
	else if($returnDate->format('N') == 7)
	{
	  $returnDate->sub(new DateInterval('P1D'));$adjReturnDates = false;
	}

	//loop through all items
	$this->load->model('orderModel');
	foreach($productArr as $product)
	{
		//get item available quantity
		$dates = $this->orderModel->getAvailableQtyByItemId($product['itemId'], $startDate, $endDate);
		$dates = $dates[0];
		$st = strtotime($product['startDate']);
		$end = strtotime($product['endDate']);
		//check if item available for all days b/w start and end date
		foreach($dates as $date)
		{
			$dateTime = strtotime($date['dates']);
			if($date['availableQty'] <= 0 && ($dateTime < $st || $dateTime > $end))
			{
				$message .= $product['itemName']." is not available on ".$date['dates']."<br>";
			}
			//if we can adj collectDate or returnDate
			else if($adjReturnDates || $adjCollectDates){
				$sql = "SELECT  count(1) as 'numOfOrders'
						FROM `orderproduct` a
						where a.`itemId` = '".$product['itemId']."'
						and a.`startDate` <= '".$collectDate->format('Y-m-d')."'
						and a.`endDate` >= '".$collectDate->format('Y-m-d')."';\n";
				$sql .= "SELECT  count(1) as 'numOfOrders'
						FROM `orderproduct` a
						where a.`itemId` = ".$product['itemId']."
						and a.`startDate` <= '".$returnDate->format('Y-m-d')."'
						and a.`endDate` >= '".$returnDate->format('Y-m-d')."';\n";
				$sql .= "SELECT `numStock` FROM `itemmaster` WHERE `itemId` = '".$product['itemId']."';";
				$result = $this->bml_database->getResults($sql);
				$totalQty = $result[2][0]['numStock'];
				
				if($result[0][0]['numOfOrders'] >= $totalQty && $adjCollectDates)
				{
					$collectDate->add(new DateInterval('P1D'));$adjCollectDates = false;
				}
				if($result[1][0]['numOfOrders'] >= $totalQty && $adjReturnDates)
				{
				  $returnDate->sub(new DateInterval('P1D'));$adjReturnDates = false;
				}
			}
		}
	}
	
	if($message == ''){
		$this->db->trans_begin();
    
		$sql = "SELECT a.`orderID`, a.`orderDiscountID`, b.`discountType`, b.`discount`
				FROM `tbl_suborder` a
				LEFT JOIN discount b
				ON a.`orderDiscountID` = b.`discountId`
				WHERE a.`subOrderID` = '$subOrderID'";
		$discount = 0;
		$result = $this->bml_database->getResults($sql);
		$result = (isset($result[0]) && isset($result[0][0])) ? $result[0][0] : array();
		if(isset($result['discountType']) && $result['discountType'] = 'PERCENT')
		{
			$discount = $result['discount'];
		}
		
		$orderID = '';
		if(isset($result['orderID']))
		{
			$orderID = $result['orderID'];
		}
		else
		{
			$this->db->trans_rollback();
			$res = array("status"=>"error", "message"=>"OrderID Not Present");
			echo json_encode($res);exit();
		}
		
		$totalAmt = $discountAmt = 0;
		foreach($productArr as $product)
		{
			$sql =  "SELECT `price` 
					FROM `pricemaster` 
					WHERE `days` = 
					(
						select max(`days`) 
						from pricemaster 
						where `days` <= '$duration' 
						and `itemId` = '".$product['itemId']."' group by `itemId`
					) 
					and `itemId` = '".$product['itemId']."'";
			
			$result = $this->bml_database->getResults($sql);
			if(!isset($result[0]) || !isset($result[0][0])) {
				$this->db->trans_rollback();
				$res = array("status"=>"error", "message"=>"Price Problem");
				echo json_encode($res);exit();
			}
			
			$result = $result[0][0];
			$rentPrice = $result['price'];
			$totalAmt += ($totalRentalPrice = $rentPrice * $duration);
			if($discount > 0)
			{
				$discountAmt = $discountAmt + ($totalRentalPrice * $discount / 100);
			}
			$sql = "UPDATE `orderproduct` 
					SET `startDate` = '".$date1->format('Y-m-d')."',
						`endDate` = '".$date2->format('Y-m-d')."',
						`collectDate` = '".$collectDate->format('Y-m-d')."',
						`returnDate` = '".$returnDate->format('Y-m-d')."',
						`duration` = '$duration',
						`rentalPrice` = '$rentPrice',
						`rentalTotalPrice` = '$totalRentalPrice'
					WHERE `orderProductId` = '".$product['orderProductId']."'";
			$this->bml_database->getResults($sql);
		}
			
		$sql = "UPDATE `tbl_suborder` 
				SET `orderSubtotal` = '$totalAmt',
					`orderDiscountAmount` = '$discountAmt',
					`adminID` = '".$this->user_session->getSessionVar('customerId')."'
				WHERE `subOrderID` = '$subOrderID';
				UPDATE `ordertb` a, 
				(
					SELECT  sum(`orderSubtotal`) as orderSubtotal, 
							sum(`orderDiscountAmount`) as orderDiscountAmount, 
							`orderID` 
					FROM `tbl_suborder` 
					WHERE `orderID` = '$orderID'
				) b
				SET a.`dicountAmount` = b.orderDiscountAmount,
				SET a.`total` = b.orderSubtotal
				where a.`orderId` = b.orderID;";
		$this->bml_database->getResults($sql);
		
		//check transaction status
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => "error", "message" => $this->CI->db->_error_message()));
			exit();
		}
		else
		{
			$this->db->trans_commit();
			//$this->db->trans_rollback();
			
			$res = array("status"=>"success", "message"=>$message, 'collectDate' => $collectDate->format('d-M-Y'), 'returnDate' => $returnDate->format('d-M-Y'));
			echo json_encode($res);exit();
		}
	}
	else {
		$res = array("status"=>"error", "message"=>$message);
		echo json_encode($res);
	}
  }
  
  public function removeItemFromOrder()
  {
		$subOrderID = $this->input->post('subOrderID');
		$productIDs = $this->input->post('productID');
		
		
		$this->db->trans_begin();
    
		$sql = "SELECT a.`orderID`, a.`orderDiscountID`, b.`discountType`, b.`discount`
				FROM `tbl_suborder` a
				LEFT JOIN discount b
				ON a.`orderDiscountID` = b.`discountId`
				WHERE a.`subOrderID` = '$subOrderID'";
		$discount = 0;
		$result = $this->bml_database->getResults($sql);
		$result = (isset($result[0]) && isset($result[0][0])) ? $result[0][0] : array();
		if(isset($result['discountType']) && $result['discountType'] = 'PERCENT')
		{
			$discount = $result['discount'];
		}
		
		$orderID = '';
		if(isset($result['orderID']))
		{
			$orderID = $result['orderID'];
		}
		else
		{
			$this->db->trans_rollback();
			$res = array("status"=>"error", "message"=>"something went wrong");
			echo json_encode($res);exit();
		}
		
		$totalAmt = $discountAmt = 0;
		foreach($productIDs as $productID)
		{
			$sql =  "SELECT `rentalTotalPrice` from orderproduct where `itemId` = '".$productID."' and `subOrderID` = $subOrderID";
			$result = $this->bml_database->getResults($sql);
			if(!isset($result[0]) || !isset($result[0][0])) {
				$this->db->trans_rollback();
				$res = array("status"=>"error", "message"=>"something went wrong");
				echo json_encode($res);exit();
			}
			
			$result = $result[0][0];
			$rentPrice = $result['rentalTotalPrice'];
			$totalAmt += $rentPrice;
			
			if($discount > 0)
			{
				$discountAmt = $discountAmt + ($rentPrice * $discount / 100);
			}
			$sql = "delete from `orderproduct` 
					where `itemId` = '".$productID."' and `subOrderID` = $subOrderID";
			$this->bml_database->getResults($sql);
		}
			
		$sql = "UPDATE `tbl_suborder` 
				SET `orderSubtotal` = `orderSubtotal` - '$totalAmt',
					`orderDiscountAmount` = `orderDiscountAmount` - '$discountAmt',
					`adminID` = '".$this->user_session->getSessionVar('customerId')."'
				WHERE `subOrderID` = '$subOrderID';
				UPDATE `ordertb` a, 
				(
					SELECT  sum(`orderSubtotal`) as orderSubtotal, 
							sum(`orderDiscountAmount`) as orderDiscountAmount, 
							`orderID` 
					FROM `tbl_suborder` 
					WHERE `orderID` = '$orderID'
				) b
				SET a.`dicountAmount` = b.orderDiscountAmount,
				SET a.`total` = b.orderSubtotal
				where a.`orderId` = b.orderID;";
		$this->bml_database->getResults($sql);
		
		//check transaction status
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => "error", "message" => $this->CI->db->_error_message()));
			exit();
		}
		else
		{
			$this->db->trans_commit();
			//$this->db->trans_rollback();
			
			$res = array("status"=>"success", "message"=>'');
			echo json_encode($res);exit();
		}
  }
  
  public function updateStatusByCode($suborderID, $statusCode)
  {
    $this->_updateStatusByCode($suborderID, $statusCode);
    header('Location: '.$this->agent->referrer());
  }

  protected function _updateStatusByCode($suborderID, $statusCode)
  {
	
	$sql = $this->getSuborderHistoryInsertSql($suborderID);
	$sql .= $this->getOrderProductHistoryInsertSql($suborderID);
    $sql .="SELECT `orderStatusID` INTO @statusID
            FROM `tbl_order_status`
            WHERE `orderStatsCode` = '$statusCode';
            UPDATE `tbl_suborder`
            SET `status`=@statusID, 
				`date` = now(), 
				`adminID` = '".$this->user_session->getSessionVar('customerId')."'
            WHERE `subOrderID` = '$suborderID';
            UPDATE `orderproduct`
            SET `status`=@statusID
            WHERE `subOrderID` = '$suborderID';
            select `orderID` INTO @orderID
            from tbl_suborder
            where `subOrderID` = $suborderID;
            SELECT count(1) INTO @orderTmpCount
            FROM `tbl_suborder`
            WHERE `status`!= @statusID
            and   `orderID` = @orderID;
            UPDATE `ordertb`
            SET `status`= if(@orderTmpCount = 0,@statusID,`status`)
            WHERE `orderId` = @orderID;";
	
	if("COMPLETE" == $statusCode){
		$sql .= "SELECT `customerId` INTO @custID FROM `ordertb` WHERE `orderId` = @orderID;
				insert into `points_vallet`(`customerId`, `orderID`, `points`, `pointsType`)
				SELECT @custID, `subOrderID`, FLOOR((`orderSubtotal`-`orderDiscountAmount`)/50),'EARNED' FROM `tbl_suborder` WHERE `subOrderID` = '$suborderID';";
	}
	if("APPROVE" == $statusCode){
		$this->sendApprovalEmail($suborderID);
		$this->sendApprovalSms($suborderID);
	
	}
	else if("CANCEL" == $statusCode){
		$this->sendCancelEmail($suborderID);
	}
	else if("COMPLETE" == $statusCode){
		$this->sendCompleteEmail($suborderID);
		$this->sendCompleteSms($suborderID);
	}
    $this->bml_database->getResults($sql);

  }

  public function updateStatus()
  {
	$status = $this->input->post('status');

    //$returnDate = $this->input->post('returnDate');
    //$endDate = $this->input->post('endDate');
    //$startDate = $this->input->post('startDate');
    //$collectDate = $this->input->post('collectDate');
    $suborderID = $this->input->post('orderID');
    $comments = $this->input->post('comments');
    $rating = $this->input->post('rating');
    $cust_comments = $this->input->post('cust_comments');
    $pickup_cost = $this->input->post('pickup_cost');
    $drop_cost = $this->input->post('drop_cost');
    //print_r($status);
	
    if($status == "DELETE")
    {
      $sql1= "DELETE FROM `orderproduct` WHERE `subOrderID`='".$suborderID."'";
      $sql2= "DELETE FROM `tbl_suborder` WHERE `subOrderID`='".$suborderID."'";
      $this->bml_database->getResults($sql1);
      $this->bml_database->getResults($sql2);
      redirect(admin_url('orders/cancel'));
    }

	$sql =$this->getSuborderHistoryInsertSql($suborderID);
	$sql .= $this->getOrderProductHistoryInsertSql($suborderID);
    $sql .= "update customer 
			set `comment` = '$cust_comments' 
			where `customerId` = 
			(
				SELECT `customerId` 
				FROM `ordertb` 
				WHERE `orderId` = 
				(
					SELECT `orderID` 
					FROM `tbl_suborder` 
					WHERE `subOrderID` = '$suborderID'
				)
			);";
	if($status != 'SAVE')
	{
		$sql .= "SELECT `orderStatusID` INTO @approveStatusID
            FROM `tbl_order_status`
            WHERE `orderStatsCode` = '$status';
            UPDATE `tbl_suborder`
            SET `comments`='$comments',
				`rating`='$rating',
                `status`=@approveStatusID, 
				`date` = now(), 
				`adminID` = '".$this->user_session->getSessionVar('customerId')."' 
            WHERE `subOrderID` = '$suborderID';
            UPDATE `orderproduct`
            SET `status`=@approveStatusID
            WHERE `subOrderID` = '$suborderID';
            select `orderID` INTO @orderID
            from tbl_suborder
            where `subOrderID` = $suborderID;
            SELECT count(1) INTO @orderTmpCount
            FROM `tbl_suborder`
            WHERE `status`!= @approveStatusID
            and   `orderID` = @orderID;
            UPDATE `ordertb`
            SET `status`= if(@orderTmpCount = 0,@approveStatusID,`status`)
            WHERE `orderId` = @orderID;";
	}
	else {
		$sql .= "UPDATE `tbl_suborder`
				SET `comments`='$comments', 
					`rating`='$rating',
					`date` = now(), 
					`adminID` = '".$this->user_session->getSessionVar('customerId')."' 
				WHERE `subOrderID` = '$suborderID';";
	}
	if($pickup_cost)
	{
		$sql .= "insert into shipping_tb(`orderID`, `ship_type`, `ship_flg`, `paid`)
				values ($suborderID, 'P', 1, '".$pickup_cost."')";
	}
	else if($drop_cost)
	{
		$sql .= "insert into shipping_tb(`orderID`, `ship_type`, `ship_flg`, `paid`)
				values ($suborderID, 'R', 1, '".$drop_cost."')";
	}
	if("COMPLETE" == $status){
		$sql .= "SELECT `customerId` INTO @custID FROM `ordertb` WHERE `orderId` = @orderID;
				insert into `points_vallet`(`customerId`, `orderID`, `points`, `pointsType`)
				SELECT @custID, `subOrderID`, FLOOR((`orderSubtotal`-`orderDiscountAmount`)/50),'EARNED' FROM `tbl_suborder` WHERE `subOrderID` = '$suborderID';";
	}
	
	
     $this->bml_database->getResults($sql);
	//echo $sql;exit();
	
	if("APPROVE" == $status){
		$this->sendApprovalEmail($suborderID);
		$this->sendApprovalSms($suborderID);

	}
	else if("CANCEL" == $status){
		$this->sendCancelEmail($suborderID);
	}
	else if("COMPLETE" == $status){
		$this->sendCompleteEmail($suborderID);
		$this->sendCompleteSms($suborderID);
	}
	
    header('Location: '.$this->agent->referrer());
  }

  private function checkAvailProduct($productID, $date1, $date2)
  {
  
	$date1 = $date1->setTime(0,0);
	$date2 = $date2->setTime(0,0);
	
    $this->load->model('orderModel');
    $availableCountRes = $this->orderModel->getAvailableQtyByItemId($productID);

    $availableQty = (array_key_exists(0, $availableCountRes)) ? $availableCountRes[0]: array();
    $available = true;

    foreach ($availableQty as $availableQtyRow) {
        $date = date_create($availableQtyRow['dates']);
        $qty = $availableQtyRow['availableQty'];
        if(($date1 <= $date)
              && ($date2 >= $date))
        {
          if($qty <= 0)
          {
            $available = false;break;
          }
        }
    }
    return $available;
  }

}
