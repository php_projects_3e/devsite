<div class="clearfix"></div>
<br>

	
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-3 col-md-3">
			<h4 class="text-color">CONTACT US</h4> 
			<p>We are Open, Monday through Saturday, 9:30 AM to 6:30 PM.</p>
			<strong class="text-color">The best way to reach us,</strong><br>
			<div class="col-md-2  col-sm-2">
				<span class="text-color font-size-20px"><i class="fa fa-phone-square"></i>
				</span>
			</div>
			<div class="col-md-10  col-sm-10">
				1800-121-0446
			</div>
			<div class="clearfix"></div>
			<div class="col-md-2  col-sm-2">
				<span class="text-color font-size-20px"><i class="fa fa-mobile-phone"></i>
				</span>
			</div> 
			<div class="col-md-10  col-sm-10">
				+91 961.12345.28
			</div>
			<div class="clearfix"></div>
			<div class="col-md-2  col-sm-2">
				<span class="text-color font-size-20px"><i class="fa fa-envelope-o"></i>
				</span>
			</div> 
			<div class="col-md-10  col-sm-10">rentals@bookmylens.com
			</div>
			<div class="clearfix"></div>
			<div class="col-md-2  col-sm-2">
				<span class="text-color font-size-20px"><i class="fa fa-twitter-square"></i>
				</span>
			</div> 
			<div class="col-md-10  col-sm-10"> @BookMyLens
			</div>
			<div class="clearfix"></div>
			<div class="col-md-2  col-sm-2"><span class="text-color font-size-20px"><i class="fa fa-map-marker"></i></span></div>
			 <div class="col-md-10  col-sm-10">BookMyLens,<br>
			 No 745, 18th Main,<br>
			 1st A Cross 6th Block,<br>
			 Koramangala,<br>
			 Bangalore 560 095.<br></div>
		</div>
		<div class="col-sm-5 col-md-5" id="map" style="height:315px;">
			
		</div>
	     <div class="col-sm-2 col-md-2"></div>

	     <script src="https://maps.googleapis.com/maps/api/js"></script>
		<script>
		  function initialize() {
			var mapCanvas = document.getElementById('map');
			var mapOptions = {
			  center: new google.maps.LatLng(12.936883267694844,77.62115210294724),
			  zoom: 16
			}
			var map = new google.maps.Map(mapCanvas, mapOptions);
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(12.936883267694844,77.62115210294724),
				map: map,
				title: 'BookMyLens'
			  });
		  }
		  google.maps.event.addDomListener(window, 'load', initialize);
		</script>