<div class="clearfix"></div>
<br>
<div class="col-sm-2 col-md-2"></div>
<div class="col-sm-8 col-md-8">
	<div class="edit_Title text-center">Frequently Asked Questions</div>
<div class="panel-group" id="accordion">
	<?php 
	$i=0;
	foreach ($faq as $faqobject) {
		?>
    <div class="panel panel-default margin-top--2px" >
      <div class=" menu_head">
        <h4 class="panel-title font-size-13px">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo ++$i; ?>"><?php echo $faqobject->faqquestion; ?></a>
        </h4>
      </div>
      <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse ">
        <div class="menu_body"><?php echo $faqobject->faqanswer; ?></div>
      </div>
    </div>
    <?php }?>
</div>
</div>
<div class="col-sm-2 col-md-2"></div>