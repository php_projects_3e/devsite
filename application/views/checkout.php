<div class="clearfix"></div>
<br>
<div class="col-sm-1 col-md-1"></div>
<div class="col-sm-10 col-md-10">
  <div class="row">
    <div class="col-md-8">
      <?php
        $breadcrumbText = '';
        foreach ($breadcrumb as $breadcrumbRow){
          $breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];
          $breadcrumbText .= " > ";
        }
        echo rtrim($breadcrumbText, " > ");
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-8 col-md-8">
      <div class="col-sm-12 col-md-12">
        <h2>Checkout Cart <a href="<?php echo site_url('cart/view/'); ?>" class="float-right">
          <button class=" btn btn-success float-right">
            Back to Cart
          </button>
        </a></h2>
      </div>

      <div class="col-sm-12 col-md-12 padding-0px">
        <form method="post" action="<?php echo site_url('cart/checkoutsuccess');?>"  id="checkoutForm">
        <div class="col-sm-12 col-md-12">
          <table class="table">
            <thead class="color-fff background-color-c1272d" >
              <th>Item Description</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Item Price</th>
              <th> Qty </th>
              <th>Sub-Total</th>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            <?php foreach ($this->cart->contents() as $items): ?>
              <?php $options = $this->cart->product_options($items['rowid']);?>
              <tr <?php echo ($options['waitingList'] == true)? "class='bg-danger' title='waitingList'":'';?> >
                <td>
                  <?php echo $items['name']; ?><br />
                </td>
                <td>
                  <?php echo $options['startDate']; ?><br />
                </td>
                <td>
                  <?php echo $options['endDate']; ?><br />
                </td>
                <td >
                  <span class="text-color"><i class="fa fa-inr"></i></span><?php echo $this->cart->format_number($items['price']); ?>
                </td>
                <td>
                  <?php $qty=$items['qty']; ?>
                  <span id="qty_<?php echo $items['rowid']; ?>"> <?php echo $qty; ?> </span>  &nbsp;
                </td>
                <td >
                  <span class="text-color"><i class="fa fa-inr"></i></span> <span id="subtotal_<?php echo $items['rowid']; ?>"><?php echo $this->cart->format_number($items['subtotal']); ?></span>
                </td>
              </tr>
              <?php $i++; ?>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div class="col-sm-12 col-md-12">

          <hr class="hr1">
          <div class="col-sm-12 col-md-12 line-height-26px padding-bottom-5px" >
            <div class="col-sm-6 col-md-6 padding-0px">
            <div class="form-group">
                <label class="control-label col-sm-4 padding-0px font-size-12px">Pickup Address:</label>
                <div class="col-sm-8">
                    <select name="pickupAddr" id="pickupAddr" class="padding-3px-0px width-100" >
                      <?php echo $addressOption; ?>
                    </select>
                    <!-- <a title="click here to add new address" onclick="incrementData('c0860bf5673c31eb2bdce02cc7a035d3')"><b class="text-success"><i class="fa fa-plus-square"></i></b></a> -->
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="text-justify font-size-12px line-height-16px" id="pickupAddress"></div>

            </div>

            <div class="col-sm-6 col-md-6 padding-0px" >
            <div class="form-group">
                <label class="control-label col-sm-4 padding-0px font-size-12px" >Return Address:</label>
                <div class="col-sm-8">
                    <select name="returnAddr" id="returnAddr"   class="width-100 padding-3px-0px">
                      <?php echo $addressOption; ?>
                    </select>
                    <!-- <a title="click here to add new address" onclick="incrementData('c0860bf5673c31eb2bdce02cc7a035d3')"><b class="text-success"><i class="fa fa-plus-square"></i></b></a> -->
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="text-justify font-size-12px line-height-16px"   id="returnaddress"></div>
            <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <br>

          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-4 col-md-4 margin-top-66px" >
      <div class="vr hr1 background-color-c1272d" ></div>

      <div class="col-sm-12 col-md-12 text-right line-height-26px padding-bottom:5px">
        <table class="table">
          <tbody>
    		<tr>
				<td colspan="2" class="border-top-1px-solid-C1272D">Sub Total :<br><span class="text-danger font-size-10px" >(excluding products in waitingList)</span></td>
				<td class="border-top-1px-solid-C1272D" >
					<span class="text-color"><i class="fa fa-inr"></i></span> 
					<span id="subtotal"><?php echo $this->cart->format_number($this->cart->subtotal()); ?></span>
				</td>
			</tr>
            <tr>
				<td></td><td>Discounts :</td>
				<td>
					<span class="text-color"><i class="fa fa-inr"></i></span> 
					<span id="discount"><?php echo $this->cart->format_number($this->cart->cartDiscount()); ?></span>
				</td>
			</tr>
            <tr id="shipping1">
				<td colspan="2">Shipping Cost:</td>
				<td>
					<span class="text-color"><i class="fa fa-inr"></i></span> 
					<span id="shippingCost"><?php echo $this->cart->format_number(0); ?></span>
				</td>
			</tr>
            <div id="shippingCost1" ></div>
            <div id="returnCost" ></div>
            <tr>
				<td colspan="2"><b class="font-size-16px">Grand Total :</b><br><span class="text-danger font-size-10px">(excluding products in waitingList & Shipping)</span></td>
				<td>
					<span class="text-color"><i class="fa fa-inr"></i></span> 
					<span id="total"><?php echo $this->cart->format_number($this->cart->total()); ?></span>
				</td>
			</tr>
            <tr class="<?php echo ($this->user_session->getSessionVar('emailID') == "agnesh2102@gmail.com" || $this->user_session->getSessionVar('emailID') == "rajiv.ranjan@payu.in")? '':'';?>">
				<td colspan="1"><b class="font-size-16px">Payment Type :</b></td>
				<td colspan="2">
					<select name="paymentType" id="paymentType">
						<option selected value="PUM">Net Banking</option>
						<option value="PUM">Credit Card / Debit Card</option>
            <option value="INMJ">Pay with Instamojo</option>
						<option value="BT">Pay Later</option>
					</select>
					<input type="hidden" name="paymentAmountType" value="ODR" id="paymentAmountTypeInput">
				</td>
			</tr>
          </tbody>
        </table>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-12 col-md-12 text-center">
        <input onclick="return(validate());" class="btn btn-success"  value="Place Order">
      </div>
      </form>
    </div>
  </div>
</div>
<div class="col-sm-1 col-md-1"></div>
<div class="clearfix"></div>

<div id="placeorderPUMModal" class="modal fade bs-example-modal-sm bs-example-modal-lg bs-example-modal-xs bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-md modal-xs ">
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
		
      <div class="clearfix"></div>
      <br>
      <div class="col-sm-12 col-md-12">
        <p><b>Please select Payment Amount</b></p>
		<select onchange="changePaymentAmount(this)" class="text-center">
			<option value="ODR">One Day Rental</option>
			<option value="FR">Full Rental</option>
		</select>
        <div class="clearfix"></div>
        <br>
        <div class="text-center">
          <button type="button" onclick="$('#checkoutForm').submit();" class="btn btn-success  text-center" data-dismiss="modal"> Proceed </button>
        </div>
        <div class="clearfix"></div>
        <br>
      </div>
      <br>
      <div class="clearfix"></div>
    </div>
  </div>
</div>

<!-- Paying with Instanmojo -->
<div id="placeorderINMJModal" class="modal fade bs-example-modal-sm bs-example-modal-lg bs-example-modal-xs bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-md modal-xs ">
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
		
      <div class="clearfix"></div>
      <br>
      <div class="col-sm-12 col-md-12">
        <p><b>Please select Payment Amount</b></p>
		<select onchange="changePaymentAmount(this)" class="text-center">
			<option value="ODR">One Day Rental</option>
			<option value="FR">Full Rental</option>
		</select>
        <div class="clearfix"></div>
        <br>
        <div class="text-center">
          <button type="button" onclick="$('#checkoutForm').submit();" class="btn btn-success  text-center" data-dismiss="modal"> Proceed </button>
        </div>
        <div class="clearfix"></div>
        <br>
      </div>
      <br>
      <div class="clearfix"></div>
    </div>
  </div>
</div>


<div id="placeorderBTModal" class="modal fade bs-example-modal-sm bs-example-modal-lg bs-example-modal-xs bs-example-modal-md" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-md modal-xs ">
    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
      <div class="clearfix"></div>
      <br>
      <div class="col-sm-12 col-md-12">
        <p><b>IMPORTANT:</b> Please make 1 day rental/full amount as advance payment in next 48hrs to confirm the same. </p>
        <p>Please transfer the advance amount to <br>
        
          <div class="col-md-4">NAME : </div>
          <div class="col-md-8"><b>Creative Capture Pvt Ltd (BOOK MY LENS)</b></div>
          <div class="clearfix"></div>
          <div class="col-md-4">BANK : </div>
          <div class="col-md-8"><b>HDFC Bank , KORAMANGALA</b></div>
          <div class="clearfix"></div>
          <div class="col-md-4">IFSC code : </div>
          <div class="col-md-8"><b>HDFC0000053</b></div>
          <div class="clearfix"></div>
          <div class="col-md-4">Current A/C #.: </div>
          <div class="col-md-8"><b>50200005019560</b></div>
          <div class="clearfix"></div>
        
        <div class="clearfix"></div>
        <br>
        <div class="text-center">
          <button type="button" onclick="$('#checkoutForm').submit();" class="btn btn-success  text-center" data-dismiss="modal"> OK </button>
        </div>
        <div class="clearfix"></div>
        <br>
      </div>
      <br>
      <div class="clearfix"></div>
    </div>
  </div>
</div>

<!-- Modal for Please Select Pickup Address -->
<div class="modal fade" id="selectPickupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog width-300px" role="document">
    <div class="modal-content" >
      <div class="modal-header bg-warning border-bottom-1px-solid-fff background-color-#fff">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-danger text-uppercase" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">

        <p class="text-center text-danger"><b>Please Select Pickup Address.</b></p>
        <br>


        <div class="text-center">

          <button class="btn btn-danger btn-sm" onclick="$('#pickupAddr').focus();" data-dismiss="modal" aria-label="Close">OK</button>
        </div>

        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>
<!-- Modal for Please Select Return Address -->
<div class="modal fade" id="selectReturnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
  <div class="modal-dialog width-300px" role="document">
    <div class="modal-content" >
      <div class="modal-header bg-warning border-bottom-1px-solid-fff background-color-#fff">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-danger text-uppercase" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">

        <p class="text-center text-danger"><b>Please Select Return Address.</b></p>
        <br>


        <div class="text-center">

          <button class="btn btn-danger btn-sm" data-dismiss="modal" aria-label="Close">OK</button>
        </div>

        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">

function changePaymentAmount(ele)
{
	value = $(ele).find('option:selected').val();
	$('#paymentAmountTypeInput').val(value);
}
function validate()
{
  if( document.getElementById("pickupAddr").value == "-1" )
   {
    $('#pickupAddr').focus(function(){
        $(this).css("border", "2px solid #C1272D");
    });
     $('#selectPickupModal').modal('show');
     return false;
   }
	if( document.getElementById("returnAddr").value == "-1" )
	{
		$('#returnAddr').focus(function(){
			$(this).css("border", "2px solid #C1272D");
		});
		$('#selectReturnModal').modal('show');
		return false;
	}
	if(document.getElementById("paymentType").value == "PUM")
	{
		$('#placeorderPUMModal').modal('show');
	} 
  else if(document.getElementById("paymentType").value == "INMJ")
	{
		$('#placeorderINMJModal').modal('show');
	}
	else 
	{
		$('#placeorderBTModal').modal('show');
	}
   return( true);
}
</script>


<script>
$(document).ready(function(){

    pickupCost = returnCost = 0;
    $("#pickupAddr").on("change", function(){

      var pickupAddr = document.getElementById("pickupAddr").value;

      if(pickupAddr!='-1')
      {

      $.ajax({
        type: "POST",
        url: '<?php echo site_url("cart/getClientAddressbyID"); ?>',
        data:{ addressID : pickupAddr},
        success: function(data)
        {
          var data = jQuery.parseJSON(data);

          shipping= data;
          pickupCost=shipping.shippingCost;
          shippingCost = parseInt(pickupCost)+parseInt(returnCost);
          document.getElementById("shippingCost").innerHTML = shippingCost;
          document.getElementById("shippingCost1").innerHTML="<input type='hidden' name='shipping' value='"+(shippingCost)+"'>";
          data = "<div class='col-lg-4 col-sm-4 '><b> Address: </b></div><div class='col-lg-8 col-sm-8'> "+data.addressLine1+","+data.addressLine2+",<br> " +data.addressCity+",<br>"+data.addressState+", <br>"+data.addressPin+",</div><div class='col-lg-4 col-sm-4'><b>Land# : </b></div><div class='col-lg-8 col-sm-8'> "+data.addressLandmark+"</div>";
          document.getElementById("pickupAddress").innerHTML = data;

        },
        error: function(e)
        {
          alert("fail");
        }
        });
    }
    else
    {
      $('#pickupAddr').focus();
      $('#pickupAddr').focus(function(){
        $(this).css("border", "2px solid #C1272D");
      });
      $('#selectPickupModal').modal('show');


    }
    });

    $("#returnAddr").on("change", function(){
      var returnAddr = document.getElementById("returnAddr").value;

      if(returnAddr!='-1')
      {
      $.ajax({
        type: "POST",
        url: '<?php echo site_url("cart/getClientAddressbyID"); ?>',
        data:{ addressID : returnAddr},
        success: function(data)
        {

          var data = jQuery.parseJSON(data);
          returnCost=data.shippingCost;
          var totalShippingCost= parseInt(pickupCost)+parseInt(returnCost);

          document.getElementById("returnCost").innerHTML="<input type='hidden' name='returnCost' value='"+returnCost+"'>";
          document.getElementById("shippingCost").innerHTML =totalShippingCost;

          data = "<div class='col-lg-4 col-sm-4 '><b> Address: </b></div><div class='col-lg-8 col-sm-8'> "+data.addressLine1+","+data.addressLine2+",<br> " +data.addressCity+",<br>"+data.addressState+", <br>"+data.addressPin+",</div><div class='col-lg-4 col-sm-4'><b>Land# : </b></div><div class='col-lg-8 col-sm-8'> "+data.addressLandmark+"</div>";

          document.getElementById("returnaddress").innerHTML = data;
        },
        error: function(e)
        {
          alert("fail");
        }
        });
    }
    else
    {
      $('#returnAddr').focus(function(){
        $(this).css("border", "2px solid #C1272D");
      });
      $('#selectReturnModal').modal('show');
    }

    });
});
</script>



