
<div class="clearfix"></div>
<br>
<div class="col-sm-2 col-md-2"></div>
  <div class="col-sm-8 col-md-8">
	  <div class="row">
		<div class="col-md-12">
			<?php
			  $breadcrumbText = '';
			  foreach ($breadcrumb as $breadcrumbRow){
				$breadcrumbText .= ($breadcrumbRow['link'] != '') ? "<a href = '".$breadcrumbRow['link']."'>".$breadcrumbRow['name']."</a>" :$breadcrumbRow['name'];
				$breadcrumbText .= " > ";
			  }
			  echo rtrim($breadcrumbText, " > ");
			?>
		</div>
	  </div>
    <div class="col-sm-12 col-md-12">
      <div class="col-sm-8 col-md-8">
        <h2>Change Password</h2>
      </div>
      <div class="col-sm-8 col-md-8">
		<form method="post" id="changePassForm" action="<?php echo site_url('myaccount/saveNewPassword');?>" role="form">
			<div class="form-group">
				<label>Existing Password</label>
				<input id="curpass" type="password" class="form-control width-100" required name="curPass" placeholder="Existing Password">
			</div>
			<div class="form-group">
				<label>New Password</label>
				<input id="password" type="password" class="form-control width-100" required name="newPass" placeholder="New Password">
			</div>
			<div class="form-group">
				<label>Confirm Password</label>
				<input id="password_again" type="password" class="form-control width-100" required name="retypePass" placeholder="Retype Password">
			</div>
			<div class="form-group">
				<input type="submit" id ="changePassAction" value="Change Password " class="btn btn-info fa ">
			</div>
		</form>
      </div>
		<script>
			$(function(){
				changePassForm = $('#changePassForm');
				changePassForm.validate({
				  rules: {
					password: "required",
					password_again: {
					  equalTo: "#password"
					}
				  }
				});
				changePassForm.submit(function (ev) {
					ev.preventDefault();
					$.ajax({
						type: changePassForm.attr('method'),
						url: changePassForm.attr('action'),
						data: changePassForm.serialize(),
						success: function (data) {
							var data = jQuery.parseJSON(data);
							if(data.status)
							{
								alert(Password Changed Successful);
							  window.location = "<?php echo site_url(); ?>";
							}
							else{
								$('#curpass').val('');
								$('#password').val('');
								$('#password_again').val('');
								alert(data.message);
							}
						}
					});
					return false;
				});
			});
			
		</script>
    </div>
<div class="col-sm-2 col-md-2"></div>
<div class="clearfix"></div>
<br>