
<div class="col-md-12" style="border: 5px solid #C1272D;  margin-top: 10px;height: 500px !important;">
  <p>
    Select Rent Dates:
  </p>
  <div class="datepicker"></div>
  <div class="clearfix"></div>
  <br>
  <table class="table table-striped table-bordered table-condensed">
    <tr><th>From:</th><td><label id="ldate1"></label></td></tr>
    <tr><th>To:</th><td><label id="ldate2"></label></td></tr>
    <tr><th>Number of days:</th><td><label id="ldays"></label></td></tr>
    <tr><th>Rental Prices</th><td><label id="lprice"></label></td></tr>
  </table>

  <form id="hiddenForm" name="myForm" method="post" action="" onsubmit="return validateForm()">
    <input type="hidden" name="orderID" value="">
    <input type="hidden" id="date1"  name="date1" value="">
    <input type="hidden" id="date2" name="date2" value="">
    <input type="hidden" id="days" name="days" value="">
    <input type="hidden" id="price" name="price" value="">
    <input type="hidden" id="productName" name="productName" value="<?php echo $productDetails['itemName'];?>">
    <input type="hidden" id="productID" name="productID" value="<?php echo $productDetails['itemId'];?>">
  </form>
  <button onclick="add();">Add </button>
</div>


<script type="text/javascript">
  var priceArray = <?=json_encode($priceListArr)?>;
  var unAvailableDates = <?=json_encode($unAvailableDates);?>;
  var holidayList = <?= json_encode($holidayList);?>;
  var waitingList = false;

  //rent page, select rental page
  $(".datepicker").datepicker({
      minDate: 0,
      numberOfMonths: 2,
      dateFormat: 'dd/M/yy',
      todayHighlight:false,
      beforeShowDay: function(date) {
        var date1 = $.datepicker.parseDate('dd/M/yy', $("#date1").val());
        var date2 = $.datepicker.parseDate('dd/M/yy', $("#date2").val());
        dateStr = date.getFullYear() + "-" + ('0' + (date.getMonth()+1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2);
        var cssClass = "";

        if(date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)))
        {
          cssClass = "dp-highlight";
          jQuery.each(unAvailableDates, function(index, dateVal){
            var date = $.datepicker.parseDate("yy-mm-dd", dateVal);
            if(date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2))){
              cssClass = "waiting";
              //set waiting flag
              waitingList = true;
            }
          });
          return [true, cssClass];
        }
        else if(jQuery.inArray( dateStr, unAvailableDates) !== -1)
        {
          return [false, "unavailable"];
        }
        else if(jQuery.inArray( dateStr, holidayList) !== -1)
        {
          return [false, "closed"];
        }
        return [true,""];
      },
      onSelect: function(dateText, inst) {
        //reset waiting flag;
        //it will be set in beforeShowDay() function, if product is not available for selected dates
        waitingList = false;
        var date1 = $.datepicker.parseDate('dd/M/yy', $("#date1").val());
        var date2 = $.datepicker.parseDate('dd/M/yy', $("#date2").val());
        var selectDate = $.datepicker.parseDate('dd/M/yy', dateText);
        if (!date1 || date2) {
          $("#date1").val(dateText);
          $("#date2").val();

          $("#ldate1").text(dateText);
          $("#ldate2").text(dateText);

          $(this).datepicker();
          $('#days').val(1);
          $('#ldays').text(1);
          $('#price').val(priceArray[1]);
          $('#lprice').text("INR "+priceArray[1]);

        } else if(selectDate >= date1){
          $("#date2").val(dateText);
          $("#ldate2").text(dateText);
          $(this).datepicker();
          var days = ((selectDate-date1)/(1000 * 60 * 60 * 24)) + 1;
          $('#days').val(days);
          $('#ldays').text(days);
          var price = priceArray[1];
          for(i=days; i>0;i--)
          {
            if(priceArray[i] != undefined)
            {
              price = priceArray[i];
              break;
            }
          }
          price *= days;
          $('#price').val(price);
          if(price == 0)
          {
            $('#lprice').text("Contact Us");
          }
          else
          {
            $('#lprice').text("INR "+price);
          }
        } else {
          $("#date1").val("");
          $("#date2").val("");
          $("#ldate1").text("");
          $("#ldate2").text("");
          $('#days').val(0);
          $('#ldays').text("");
          $('#price').val("");
          $('#lprice').text("");
        }

      }

    });

  function add(){
    if(!validateForm()) return;

     window.parent.closeModal();
     window.parent.addProduct($("#productID").val(), $('#productName').val(), $('#date1').val(), $('#date2').val(), $('#days').val(), $("#price").val());
     $('#uiFrame', parent.document).attr('src', '');
  }

  function validateForm() {
    var x = document.forms["myForm"]["date1"].value;
    var y = document.forms["myForm"]["date2"].value;
    if (x == null || x == "") {
      alert('Please select dates');
      return false;
    }
    if(y == null || y == "")
    {
      document.forms["myForm"]["date2"].value = document.forms["myForm"]["date1"].value;
    }
    return true;
  }

  //override the addClass function
  var originalAddClassMethod = jQuery.fn.addClass;
  jQuery.fn.addClass = function(){
      // Execute the original method.
      var result = originalAddClassMethod.apply( this, arguments );
      return result;
  }
</script>
