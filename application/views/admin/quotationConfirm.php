<div class="box">
    <div class="box-body">
      <div class="col-lg-12">
        <form class="form-horizontal" action="generateQuotation" method="post">
          <input type="hidden" name="clientNumber" value="<?php echo $clientNumber; ?>">
          <input type="hidden" name="pickAddrID" value="<?php echo $pickAddrID; ?>">
          <input type="hidden" name="returnAddrID" value="<?php echo $returnAddrID; ?>">
          <input type="hidden" id="valueJson" name="valueJson" value="">
          <div class="col-lg-2"><b>Client Details</b></div>
          <div class="col-lg-4"><?php echo $client; ?></div>
          <div class="clearfix"></div>
          <div class="hide">
		  <div class="col-lg-2"><b>Pickup Address</b></div>
          <div class="col-lg-10"><?php echo $pickAddr; ?></div>
          <div class="clearfix"></div>
          <div class="col-lg-2"><b>Return Address</b></div>
          <div class="col-lg-10"><?php echo $returnAddr; ?></div>
          <div class="clearfix"></div>
		  </div>
		  <div class="col-lg-2">
            Name
          </div>
		  <div class="col-lg-10"><?php echo $name; ?></div>
          <div class="col-lg-2">
            Address
          </div>
		  <div class="col-lg-10"><?php echo $address; ?></div>
          <div class="clearfix"></div>
		  
          <br>
          <table class="table" id="orderTable">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Days</th>
              <th>Price</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach ($orderDetails as $orderDetailsRow) { ?>
            <tr>
              <td><?php echo $orderDetailsRow['itemId'];?></td>
              <td><?php echo $orderDetailsRow['itemName'];?></td>
              <td><?php echo $orderDetailsRow['startDate'];?></td>
              <td><?php echo $orderDetailsRow['endDate'];?></td>
              <td><?php echo $orderDetailsRow['days'];?></td>
              <td><?php echo $orderDetailsRow['price'];?></td>
              <td><?php echo $orderDetailsRow['avail'];?></td>
            </tr>
          <?php } ?>
          </tbody>
          </table>
          <br>
          <input type="submit" onclick="return formSubmit();" value="Generate Quotation">
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
  function formSubmit()
  {
    var json = '{';
    var otArr = [];
    var tbl2 = $('#orderTable tbody tr').each(function(i) {
      x = $(this).children();
      var itArr = [];
      x.each(function() {
         itArr.push('"' + $(this).text() + '"');
      });
      otArr.push('"' + i + '": [' + itArr.join(',') + ']');
    })
    json += otArr.join(",") + '}';
    $('#valueJson').val(json);
    return true;
  }
</script>
