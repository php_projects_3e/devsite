<div class="box">
  <div class="box-body">
    <div class="col-lg-12">
      <table class="table table-bordered table-responsive table-striped datatable paymentTable">
        <thead>
          <tr>
            <th>OrderNumbers</th>
            <th>Total Order Amount</th>
            <th>Paid Amount</th>
            <th>Pending</th>
            <th><a href = "<?php echo admin_url('payments/addNew'); ?>" class="btn btn-primary">New Payment</a></th>
          </tr>
        </thead>
        <tbody>
           <?php
              $i =0;
              foreach ($paymentDetails as $row) {
                $i++;
            ?>
            <tr class="mainrow mainrow_<?php echo $i; ?>" data-id = "<?php echo $i; ?>">
              <td><?php echo $row['orderNumbers']; ?></td>
              <td><i class="fa fa-inr"></i> <?php echo $row['total']; ?></td>
              <td><i class="fa fa-inr"></i> <?php echo $row['paid']; ?></td>
              <td><i class="fa fa-inr"></i> <?php echo $row['pending']; ?></td>
              <td><a class="btn btn-primary btn-xs"> View</a></td>
            </tr>
            <tr class ="childrow childrow_<?php echo $i; ?>" data-id = "<?php echo $i; ?>">
              <td colspan="6" >
                <?php if(array_key_exists('details', $row)){ ?>
                <table class="table">
                  <thead class="font-size-12px">
                    <tr>
                      <th>PaymentType</th>
                      <th>Bank</th>
                      <th>Branch</th>
                      <th>Reference #</th>
                      <th>Amount</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Approved By</th>
                    </tr>
                  </thead>
                  <tbody class="font-size-12px">
                     <?php foreach ($row['details'] as $items) { ?>
                     <tr>
                      <td><?php echo $items['typeDisplayName']; ?></td>
                      <td><?php echo $items['bankName']; ?></td>
                      <td><?php echo $items['branch']; ?></td>
                      <td><?php echo $items['referenceNumber']; ?></td>
                      <td><i class="fa fa-inr"></i><?php echo $items['amount']; ?></td>
                      <td> <?php echo dateFromMysqlDate($items['date']); ?></td>
                      <td><?php echo ($items['status']) ? 'Approved' : 'UnApproved'; ?></td>
                      <td><?php echo ($items['adminID'] == -1) ? '<a href='.admin_url('payments/approve/'.$items['paymentID']).'>Approve</a>' : $items['adminID']; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <?php } else {?>
                  No Payment Received
                <?php } ?>

              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>


<script type="text/javascript">
  $(function(){
    $('.childrow').hide();
    $('.mainrow .btn').on('click', function(){
      id = $(this).parent().parent().data('id');
      if( $('.childrow_'+id).is(':visible'))
      {
        $(this).html('show');
        $('.childrow_'+id).hide();
      }
      else
      {
        $(this).html('hide');
        $('.childrow_'+id).show();
      }
    });

window.alert = (function() {
    var nativeAlert = window.alert;
    return function(message) {
        window.alert = nativeAlert;
        message.indexOf("DataTables warning") === 0 ?
            console.warn(message) :
            nativeAlert(message);
    }
})();

  });

  function showPaymentForm($orderID)
  {
    $('#paymentForm').removeClass('hide');
    $('#paymentDiv').addClass('hide');
  }

  function cloasForm()
  {
    $('#paymentDiv').removeClass('hide');
    $('#paymentForm').addClass('hide');
  }

</script>
