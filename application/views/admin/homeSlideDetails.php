          <div>
            <div>
              <div class="box">
                <div class="box-body">
                  <form action="<?php echo admin_url('products/insertHomeSlideDetails') ;?>" method="POST" class="form-horizontal" enctype="multipart/form-data">

                      <div class="col-lg-6 col-sm-6">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Title :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <input type='text' name='slidetitle'  class="form-control" value="<?php echo $tableRows[0]['popupTitle']; ?>">
                        </div>
                        <div class="clearfix"></div>

                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Content :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <textarea type='text' name='slidecontent' rows='3' class="form-control"><?php echo $tableRows[0]['popupDesc']; ?></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Item Name:</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <select id="combobox" class="form-control" name="slideItemID">
                            <?php foreach ($itemResult as $row) { ?>
                            <?php $selected="";
                                  if($row['itemId']==$tableRows[0]['itemId'])
                                  {
                                    $selected='selected';
                                  }
                            ?>
                            <option value="<?php echo $row['itemId']; ?>" <?php echo $selected; ?> ><?php echo $row['itemName']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-6 col-sm-6">
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide BG-Image :</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <div  class="min-height-50px border-2px-solid-black">
                              <div id="effect-2" class="effects clearfix">
                                <div class="img">
                                    <img src="<?php echo site_url('images/sliderImages/'.$tableRows[0]['popupItemImage']);?>" class="img-responsive">
                                    <div class="overlay">
                                        <a href="#" class="expand" data-toggle="modal" data-target="#slideBGImage"><i class="fa fa-cloud-upload"></i></a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 text-right padding-top1">
                          <b>Slide Item Image:</b>
                        </div>
                        <div class="col-lg-8 col-sm-8 padding-top1">
                          <div class="border-2px-solid-black min-height-100px">
                              <div id="effect-2" class="effects clearfix">
                                <div class="img">
                                    <img src="<?php echo site_url('images/sliderImages/'.$tableRows[0]['popupImage']);?>" class="img-responsive">
                                    <div class="overlay">
                                        <a href="#" class="expand" data-toggle="modal" data-target="#slideItemImage"><i class="fa fa-cloud-upload"></i></a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </div>

                      </div>
                      <div class="clearfix"></div>
                      <hr>
                      <h1 class="text-center"><button class="btn btn-primary" type="submit">SAVE</button> <button class="btn btn-warning" type="reset">RESET</button>  </h2>
                   </form>
                   <div class="clearfix"></div>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>

      </div>



<!-- Modal for upload slideBGImage -->
<div class="modal fade" id="slideBGImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">UPLOAD HOME SLIDE BACKGROUND IMAGE</h4>
      </div>
      <div class="modal-body">
        <form  action="<?php echo admin_url('products/updateHomeSlideBGImage') ;?>" method="POST" enctype="multipart/form-data">
          <div class="col-lg-6">
            <input type="file" name="slideBGImage"  class="form-control" required>
          </div>
            <input type="hidden" name='popupId' value='<?php echo $tableRows[0]['popupId'];?>'>
          <div class="col-lg-6"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

<!-- Modal for upload slideITEMImage -->
<div class="modal fade" id="slideItemImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">UPLOAD HOME SLIDE ITEM IMAGE</h4>
      </div>
      <div class="modal-body">
        <form  action="<?php echo admin_url('products/updateHomeSlideItemImage') ;?>" method="post" enctype="multipart/form-data">
          <div class="col-lg-6">
            <input type="file" name="slideItemImage"  class="form-control" required>
          </div>
            <input type="hidden" name='popupId' value='<?php echo $tableRows[0]['popupId'];?>'>
          <div class="col-lg-6"><button type='submit' class="btn btn-primary"> Submit </button></div>
        </form>
        <div class="clearfix"></div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
      itemList = <?php echo json_encode($itemList);?>;
        $( "#itemAutoComplete" ).autocomplete(
          {
            source:itemList
          });

        function custom_source_client(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(clientList, function(value) {
                return matcher.test(value.label)
                        || matcher.test(value.number)
                          || matcher.test(value.email);
            }));
        }
      </script>






