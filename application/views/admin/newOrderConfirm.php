<div class="box">
    <div class="box-body">
      <div class="col-lg-12">
        <form class="form-horizontal" action="insertNewOrder" method="post">
          <input type="hidden" name="clientNumber" value="<?php echo $clientNumber; ?>">
          <input type="hidden" name="pickAddrID" value="<?php echo $pickAddrID; ?>">
          <input type="hidden" name="returnAddrID" value="<?php echo $returnAddrID; ?>">
          <input type="hidden" id="valueJson" name="valueJson" value="">
          <div class="col-lg-2"><b>Client Details</b></div>
          <div class="col-lg-4"><?php echo $client; ?></div>
          <div class="clearfix"></div>
          <div class="col-lg-2"><b>Pickup Address</b></div>
          <div class="col-lg-10"><?php echo $pickAddr; ?></div>
          <div class="clearfix"></div>
          <div class="col-lg-2"><b>Return Address</b></div>
          <div class="col-lg-10"><?php echo $returnAddr; ?></div>
          <div class="clearfix"></div>
          <br>
		  <?php
			$earnedCost = (array_key_exists('EARNED', $points_vallet_summary)? $points_vallet_summary['EARNED']: "0");
			$spentCost = (array_key_exists('SPENT', $points_vallet_summary)? $points_vallet_summary['SPENT']: "0");
		  ?>
			<div class="orderHeading line-height-150" style="font-size: x-large;">Order Points</div>
			<div class="hrstyle" style="0 10px 10px 10px;width: auto;"></div>
			<div class="margin-left-10px font-size-16px">Points earned till dates</div>
			<div class="font-weight-bold margin-left-10px font-size-16px">Rs.<?php echo $earnedCost; ?></div>
			<div class="hrstyle" style="margin: 0 10px 10px 10px;width: auto;"></div>
			<div class="margin-left-10px line-height-150 font-size-16px">Points used till dates</div>
			<div class="font-weight-bold margin-left-10px font-size-16px">Rs.<?php echo $spentCost; ?></div>

			<?php if($promocode == null) { ?>
				<?php if(($earnedCost - $spentCost) >= 250 ) { ?>
					<div class="margin-left-10px font-size-14px font-weight-bold">
						<button onclick="generateDiscount()" class="btn btn-danger" style="margin: 10px 0px;padding: 10px;">
							Generate Discount Voucher (Rs 250)
						</button>
					</div>
				<?php } else {?>
					<div class="margin-left-10px font-size-16px">Available Voucher</div>
					<div class="font-weight-bold margin-left-10px font-size-16px">no Voucher available</div>
					<div class="margin-left-10px font-size-11px text-danger">need 250 points to generate discount voucher</div>
				<?php } ?>
			<?php }else{ ?>
				<div class="margin-left-10px font-size-16px">Available Voucher</div>
				<div class="font-weight-bold margin-left-10px font-size-16px"><?php echo $promocode; ?></div>
				<!--<div class="margin-left-10px font-size-11px text-danger">need 250 points to generate discount voucher</div>-->
			<?php } ?>
			<div class="hrstyle" style="margin: 0 10px 10px 10px;width: auto;"></div>
		  
		  <div class="clearfix"></div>
          <div class="col-lg-2"><b>Coupon</b></div>
          <div class="col-lg-10"><input type="text" name="coupon"></div>
          <div class="clearfix"></div>
          
		  <table class="table" id="orderTable">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Days</th>
              <th>Price</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach ($orderDetails as $orderDetailsRow) { ?>
            <tr>
              <td><?php echo $orderDetailsRow['itemId'];?></td>
              <td><?php echo $orderDetailsRow['itemName'];?></td>
              <td><?php echo $orderDetailsRow['startDate'];?></td>
              <td><?php echo $orderDetailsRow['endDate'];?></td>
              <td><?php echo $orderDetailsRow['days'];?></td>
              <td><?php echo $orderDetailsRow['price'];?></td>
              <td><?php echo $orderDetailsRow['avail'];?></td>
            </tr>
          <?php } ?>
          </tbody>
          </table>
          <br>
          <input type="submit" onclick="return formSubmit();" value="Reserve">
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
  function formSubmit()
  {
    var json = '{';
    var otArr = [];
    var tbl2 = $('#orderTable tbody tr').each(function(i) {
      x = $(this).children();
      var itArr = [];
      x.each(function() {
         itArr.push('"' + $(this).text() + '"');
      });
      otArr.push('"' + i + '": [' + itArr.join(',') + ']');
    })
    json += otArr.join(",") + '}';
    $('#valueJson').val(json);
    return true;
  }
</script>
