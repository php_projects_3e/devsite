<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Bml_database
{
    private $CI;
    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->db->query("SET time_zone='+5:30'");
    }
    public function getResults($SqlCommand)
    {
        $k = 0;
        $arr_results_sets = array();
        
        //echo $SqlCommand;
        if (mysqli_multi_query($this->CI->db->conn_id, $SqlCommand)) {
            do {
                $result = mysqli_store_result($this->CI->db->conn_id);
                //echo $result;
                if ($result) {
                    $l = 0;
                    while ($row = $result->fetch_assoc()) {
                        $arr_results_sets[ $k ][ $l ] = $row;
                        $l++;
                    }
                }
                $k++;
            } while (mysqli_more_results($this->CI->db->conn_id) && mysqli_next_result($this->CI->db->conn_id));
        }
        else
        {
            $arr_results_sets[0]=mysqli_error($this->CI->db->conn_id);
        }
        return $arr_results_sets;
    }
}
