<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bml_generate_json {

    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('file');
    }


    public function generateFAQs()
    {
      $this->CI->load->model('commonModel');
      $resultSet = $this->CI->commonModel->getFAQs();

      $jsonFileName = FCPATH."json/faqs.json";
      echo $jsonFileName;
      echo write_file($jsonFileName,json_encode($resultSet[0]),'w');

    }

    public function generateRentMenu()
    {
      $this->CI->load->model('commonModel');
      $resultSet = $this->CI->commonModel->getBrandsForAllCategory();
      $rentMenu = [];
      foreach ($resultSet[0] as $resultSetRow) {
        $rentMenu[$resultSetRow['category']][] = $resultSetRow['brand'];
      }

      $jsonFileName = FCPATH."json/rentMenu.json";
      echo $jsonFileName;
      echo write_file($jsonFileName,json_encode($rentMenu),'w');

    }

}
